

#import <UIKit/UIKit.h>

@interface StudentHostelViewController : UIViewController
- (IBAction)roomRequestBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *hostelRoomReqLbl;
@property (weak, nonatomic) IBOutlet UILabel *hostelChangeReqLbl;
@property (weak, nonatomic) IBOutlet UILabel *myHostelLbl;
@property (weak, nonatomic) IBOutlet UILabel *hostelExitLbl;


@end
