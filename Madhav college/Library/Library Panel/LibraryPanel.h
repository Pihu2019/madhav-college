#import <Foundation/Foundation.h>

@interface LibraryPanel : NSObject

@property (nonatomic,strong) NSString *nameStr,*isbnStr,*authorStr,*editionStr,*publisherStr,*numberOfBooksStr,*booksAvailableStr;

@end
