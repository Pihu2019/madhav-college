
#import <Foundation/Foundation.h>

@interface LibraryMyLog : NSObject

@property (nonatomic,strong) NSString *booknameStr,*issuedDateStr,*dueDateStr,*fineAmtStr,*statusStr;

@end
