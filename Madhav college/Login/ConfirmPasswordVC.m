

#import "ConfirmPasswordVC.h"
#import "Constant.h"
#import "Base.h"
#import "StudentViewController.h"
#import "TeacherViewController.h"
#import "ChildListViewController.h"
@interface ConfirmPasswordVC ()

@end

@implementation ConfirmPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
         confirmPassword.delegate=self;
         newpassword.delegate=self;
         currentPassword.delegate=self;
    
    self.submitBtn.layer.masksToBounds=YES;
    self.submitBtn.layer.cornerRadius=8;
    

}
- (IBAction)submitBtnClicked:(id)sender {
    
    UIActivityIndicatorView *indicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
    indicator.center=self.view.center;
    [self.view addSubview:indicator];
    indicator.tintColor=[UIColor redColor];
    indicator.backgroundColor=[UIColor lightGrayColor];
    [indicator bringSubviewToFront:self.view];
    

    [indicator startAnimating];
    if(self.currentPassword.text.length==0 && self.newpassword.text.length==0 && self.confirmPassword.text.length==0)
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Enter all credentials" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [indicator stopAnimating];
        
        [self presentViewController:alertView animated:YES completion:nil];
        
    }
    if(self.currentPassword.text.length==0)
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"First enter your current password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [indicator stopAnimating];
        
        [indicator stopAnimating];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
    if(self.newpassword.text.length==0)
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"First Enter your new Password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [indicator stopAnimating];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
    if(self.confirmPassword.text.length==0)
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"First enter your confirm password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [indicator stopAnimating];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
    if(self.confirmPassword.text != self.newpassword.text)
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Please enter correct confirm password" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [indicator stopAnimating];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
    
    if(self.currentPassword.text.length>0 && self.newpassword.text.length>0 && self.confirmPassword.text.length>0)
    {
        NSLog(@"hi");
        
        [self forceDataParsing];
    }
}
-(void)forceDataParsing{
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"username==%@",username);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"branchid==%@",branchid);

    
    NSString *mainstr1=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:force_password]];
    
    NSDictionary *parameterDict = @{
                                    @"tag":@"forcepassword",
                                    @"username":username,
                                    @"password":self.currentPassword.text,
                                    @"newpassword":self.newpassword.text,
                                    @"branch_id":branchid,
                                    @"dbname":dbname
                                    };

    
    [Constant executequery:mainstr1 strpremeter:parameterDict
                 withblock:^(NSData * dbdata, NSError *error) {
                     NSLog(@"*****data:%@",dbdata);
                     if (dbdata!=nil) {
                         NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
                         NSLog(@"response data:%@",maindic);
                         
                         self->_tag=[maindic objectForKey:@"tag"];
                         self->_success=[maindic objectForKey:@"success"];
                         self->_error=[maindic objectForKey:@"error"];
                         
                         NSLog(@"***tag==%@& ***success=%@",self->_tag,self->_success);
                         
                         
                         [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                             
                         }];
                         if ([self->_success isEqual:@"0"]) {
                             
                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:nil message:@"Entered credential is incorrect" preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* ok = [UIAlertAction
                                                  actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action)
                                                  {
                                                      [alertView dismissViewControllerAnimated:YES completion:nil];
                                                      
                                                  }];
                             
                             [alertView addAction:ok];
                             [self presentViewController:alertView animated:YES completion:nil];
                         }
                         else{
                             NSLog(@"***confirm password reset successfully***");
                             
                             [self navigatingFromLogin];
                         }
                     }
                 }];
}

-(void)navigatingFromLogin{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"***groupname==%@",groupname);
    
    if (groupname!=NULL)
    {
        if ([groupname isEqual:@"Student"])
        {
            StudentViewController *admin = [self.storyboard instantiateViewControllerWithIdentifier:@"ToStudent"];
            
            [self.navigationController pushViewController:admin animated:YES];
        }
        else if ([groupname isEqual:@"Parent"])
        {
            ChildListViewController *child = [self.storyboard instantiateViewControllerWithIdentifier:@"ToParent"];
            
            [self.navigationController pushViewController:child animated:YES];
        }
        else
        {
            TeacherViewController *user = [self.storyboard instantiateViewControllerWithIdentifier:@"ToTeacher"];
            
            [self.navigationController pushViewController:user animated:YES];
        }
        
    }
    else
    {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Alert!" message:@"Entered credential is incorrect" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertView dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alertView addAction:ok];
        [self presentViewController:alertView animated:YES completion:nil];
        
    }
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.currentPassword resignFirstResponder];
    [self.newpassword resignFirstResponder];
    [self.confirmPassword resignFirstResponder];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.currentPassword)
    {
        [textField resignFirstResponder];
        [self.newpassword becomeFirstResponder];
    }
    if(textField==self.newpassword)
    {
        [textField resignFirstResponder];
        [self.confirmPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return  YES;
}

@end
