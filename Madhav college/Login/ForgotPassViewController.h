

#import <UIKit/UIKit.h>

@interface ForgotPassViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic,strong) NSString *success,*error,*tag,*successMsg,*errorMsg;
@property (weak, nonatomic) IBOutlet UILabel *submitTxt;

@end
