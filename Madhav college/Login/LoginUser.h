

#import <Foundation/Foundation.h>

@interface LoginUser : NSObject
@property (strong,nonatomic) NSString *gid,*firstname,*lastname,*mobile,*email,*schoolAdmissionNo,*groupName,*password,*orgType,*stuemail,*address,*branchid,*userid,*orgId,*cyear,*futureFlag,*picId,*forcePassword,*transportLicense;

@end
