//http://api.shephertz.com/tutorial/Push-Notification-iOS/
//https://www.appcoda.com/firebase-push-notifications/

#import "AppDelegate.h"
#import "LoginViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "Firebase.h"
#import "Reachability.h"
#import <unistd.h>
#import "Base.h"
#import "Constant.h"
#import "CircularViewController.h"
#import "GalleryViewController.h"
#import "ChildListViewController.h"
#import "StudentViewController.h"
#import "TeacherViewController.h"
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@import UserNotifications;
#endif
@import Firebase;
@import FirebaseMessaging;
@interface AppDelegate ()<UNUserNotificationCenterDelegate,FIRMessagingDelegate>
#define IDIOM UI_USER_INTERFACE_IDIOM()
#define IPAD UIUserInterfaceIdiomPad
{
    NSString *fcmTokenStr;
    NSString *deviceTokenStr;
}
@end

@implementation AppDelegate
NSString *const kGCMMessageIDKey = @"gcm.message_id";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    sleep(3);
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate=self;
    application.applicationIconBadgeNumber = 0;
    
    
    
    [GMSServices provideAPIKey:@"AIzaSyDjPb690oXfgaE-YX_OlXY_olfffrb7k-A"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyDjPb690oXfgaE-YX_OlXY_olfffrb7k-A"];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:20.0/255.0 green:99.0/255.0 blue:250.0/255.0 alpha:1.0]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    [defaults synchronize];
    
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    
    UIStoryboard *storyboard = [self grabStoryboard];
    
    self.window.rootViewController = [storyboard instantiateInitialViewController];
    [self.window makeKeyAndVisible];
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"username"];
    
    
    if(savedValue==NULL)
    {
        int screenHeight = [UIScreen mainScreen].bounds.size.height;
        
        
        switch (screenHeight) {
                
            case 1366:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main2" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
            case 1024:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main2" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                
                break;
            }
                
            default:
            {
                storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                LoginViewController *loginController=[[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"identifier"];
                UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                self.window.rootViewController = navController;
                
                break;
            }
                
        }
    }
    else
    {
        [self navigatingFromLogin];
    }
    
    [self registerForRemoteNotifications];
    
    return YES;
}
-(void)navigatingFromLogin{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"groupname==%@",groupname);
    
    if (groupname!=NULL)
    {
        UIStoryboard *storyboard = [self grabStoryboard];
        if ([groupname isEqual:@"Student"])
        {
            StudentViewController *admin = [storyboard instantiateViewControllerWithIdentifier:@"ToStudent"];
            
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:admin];
            self.window.rootViewController = navController;
        }
        else if ([groupname isEqual:@"Parent"])
        {
            ChildListViewController *child = [storyboard instantiateViewControllerWithIdentifier:@"ToParent"];
            
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:child];
            self.window.rootViewController = navController;
        }
        else
        {
            TeacherViewController *user = [storyboard instantiateViewControllerWithIdentifier:@"ToTeacher"];
            
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:user];
            self.window.rootViewController = navController;
        }
    }
}

-(void)registerForRemoteNotifications {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        center.delegate = self;
        
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            
            if(!error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
            
        }];
        
    }
    
    else {
        
        // Code for old versions
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        
                                                        UIUserNotificationTypeBadge |
                                                        
                                                        UIUserNotificationTypeSound);
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                
                                                                                 categories:nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }
    
}
- (UIStoryboard *)grabStoryboard {
    
    // determine screen size
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    NSLog(@"screenheight==== %d",screenHeight);
    UIStoryboard *storyboard;
    
    switch (screenHeight) {
           
        case 1366:
            storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
            break;
            
        case 1024:
            storyboard = [UIStoryboard storyboardWithName:@"Main2" bundle:nil];
            break;
            
            
        default:
            
            storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            break;
            
    }
    
    return storyboard;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"recieve new FCM registration token: %@", fcmToken);
    
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:@"fcmToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [FIRMessaging messaging].APNSToken = deviceToken;//added
    
    NSLog(@"APNs device token retrieved: %@", deviceToken);
    NSString *deviceTokenString=[deviceToken description];
    deviceTokenString=[deviceTokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSString *str=[deviceTokenString stringByReplacingOccurrencesOfString:@"\\s" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,[deviceTokenString length])];
    NSLog(@"str==%@",str);
    
    [[NSUserDefaults standardUserDefaults] setValue:str forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"***deviceid***** = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]);
    
}
-(void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage{
    
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:notification.request.content.userInfo];
    
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    
    [self handleRemoteNotification:[UIApplication sharedApplication] userInfo:response.notification.request.content.userInfo];
    
    completionHandler();
    
}
-(void)handleRemoteNotification:(UIApplication *)application userInfo:(NSDictionary *)remoteNotif{
    
    
    NSLog(@"Handle Remote Notification Dictionary: %@", remoteNotif);
    
    NSDictionary *temp = remoteNotif[@"aps"];
    _tagNoti=[temp objectForKey:@"tag"];
    _gcmId=[[temp objectForKey:@"gcm.message_id"]stringValue];
    _category=[temp objectForKey:@"category"];
    
    NSLog(@"tag notifi%@ gcmId=%@ category=%@",_tagNoti,_gcmId,_category);
    
    UIStoryboard *main = [self grabStoryboard];
    if([_category isEqualToString:@"Circular"]) {
        CircularViewController *viewConr = [main instantiateViewControllerWithIdentifier:@"CircularViewController"];
        
        [viewConr setIsComeFrom:@"notification"];
        UINavigationController *navCtrl = [[UINavigationController alloc] initWithRootViewController:viewConr];
        
        [self.window makeKeyAndVisible];
        [navCtrl setNavigationBarHidden:NO animated:YES];
        [self.window.rootViewController presentViewController:navCtrl animated:YES completion:NULL];
        
    }
    if([_category isEqualToString:@"Gallery"]){
        
        GalleryViewController *viewConr = [main instantiateViewControllerWithIdentifier:@"GalleryViewController"];
        
        [viewConr setIsComeFrom:@"notification"];
        
        UINavigationController *navCtrl = [[UINavigationController alloc] initWithRootViewController:viewConr];
        
        [self.window makeKeyAndVisible];
        [navCtrl setNavigationBarHidden:NO animated:YES];
        [self.window.rootViewController presentViewController:navCtrl animated:YES completion:NULL];
    }
    else{
        NSLog(@"Transport notification...");
    }
    
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

@end


