
#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_indicaor startAnimating];
    self.webView.delegate = self;
    [self.webView setDelegate:self];
    self.navigationItem.backBarButtonItem.title=@"Back";
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    [[self navigationItem] setBackBarButtonItem:newBackButton];
    
    NSURLRequest *req=[NSURLRequest requestWithURL:[NSURL URLWithString:self.myURL]];
    [self.webView setScalesPageToFit:YES];
    [self.webView loadRequest:req];
    
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"REQUEST === %@",webView.request.URL);
    
    if([self.myURL isEqualToString:[NSString stringWithFormat:@"%@",webView.request.URL]]){
        [_indicaor stopAnimating];
        [_indicaor setHidden:YES];
    }
    
    if ([[NSString stringWithFormat:@"%@",webView.request.URL] containsString:@"plugin=Fees&action="]) {
        
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"string contains !");
    } else {
        NSLog(@"string does not contain");
    }
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"*****Error : %@",error);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
