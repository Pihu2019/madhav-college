

#import "Base.h"

@implementation Base
//eps trunk Branch id:565  && parent branchid::439
/*
NSString * const mainUrl = @"http://shop.eshiksa.com/appAPI_v2_trunk/";
NSString * const dbname = @"erpeshik_esh_slave_edemo_new";
NSString * const instUrl = @"http://erp.eshiksa.net/eps_trunk/eps";
NSString * const downloadUrl =@"http://erp.eshiksa.net/eps_trunk/eps";
NSString * const homeworkdownloadUrl =@"http://erp.eshiksa.net/eps_trunk/eps";
//NSString * const trackingUrl = @"http://eps.eshiksa.net/"; //LIVE
NSString * const trackingUrl = @"http://erp.eshiksa.net/eps_trunk/eps";
NSString * const pgUrl = @"http://erp.eshiksa.net/eps_trunk/eps/esh/index.php?plugin=payment&amp;action=pay&amp;txnid=";
//NSString * const subscriptionUrl = @"http://erp.eshiksa.net/eps_trunk/eps";
NSString * const subscriptionUrl = @"http://erp.eshiksa.net/eps_trunk/eps/esh/plugins/APIS/";
NSString * const createInvoiceUrl =@"http://erp.eshiksa.net/eps_trunk/eps/esh/index.php?plugin=Fees&action=createInvoicesPdf_new";
*/


NSString * const mainUrl = @"http://eshapi.eshiksa.net/appAPI_v2/";
NSString * const dbname = @"erpeshik_esh_slave";
NSString * const instUrl = @"http://eps.eshiksa.net/";
NSString * const downloadUrl = @"http://eps.eshiksa.net/esh/reports/Finance/invoices/";
NSString * const homeworkdownloadUrl = @"http://eps.eshiksa.net/";
NSString * const trackingUrl = @"https://eps.eshiksa.net/";
NSString * const pgUrl = @"http://eps.eshiksa.net/esh/index.php?plugin=payment&amp;action=pay&amp;txnid=";

NSString * const subscriptionUrl = @"http://eps.eshiksa.net/";
NSString * const createInvoiceUrl = @"http://eps.eshiksa.net/esh/index.php?plugin=Fees&action=createInvoicesPdf_new";



NSString * const auth = @"auth.php";
NSString * const pgParams = @"pgParams.php";
NSString * const savePGData_v2 = @"savePGData_v2.php";
NSString * const excess_amount = @"excess_amount.php";
NSString * const fees_v2 = @"fees_v2.php";
NSString * const homework = @"homework.php";
NSString * const hostel = @"hostel.php";
NSString * const leave = @"leave.php";
NSString * const gallery = @"gallery.php";
NSString * const attendance = @"attendance.php";
NSString * const profile = @"profile.php";
NSString * const newcircular = @"newcircular.php";
NSString * const subjectDetails = @"subjectDetails.php";
NSString * const studentclgtimetable = @"studentclgtimetable.php";
NSString * const change_pass = @"change_pass.php";
NSString * const library = @"library.php";
NSString * const empRequisition = @"empRequisition.php";
NSString * const teacherSalaryStructure = @"teacherSalaryStructure.php";
NSString * const teacher_homework_assign = @"teacher_homework_assign.php";
NSString * const empMyReport = @"empMyReport.php";
NSString * const teachersclattendance = @"teachersclattendance.php";
NSString * const teacherclgattendance = @"teacherclgattendance.php";
NSString * const teachersubmitattendance = @"teachersubmitattendance.php";
NSString * const teacherTimetable = @"teacherTimetable.php";
NSString * const push_notifications = @"push_notifications.php";
NSString * const force_password = @"force_password.php";
NSString * const etracki_payment = @"etracki_payment.php";
NSString * const currentroutelist = @"currentroutelist.php";
NSString * const versionVerification = @"versionVerification.php";
NSString * const examReports = @"examReports.php";

@end
