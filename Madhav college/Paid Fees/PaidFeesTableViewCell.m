
#import "PaidFeesTableViewCell.h"

@implementation PaidFeesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.invoiceBtn.layer.masksToBounds=YES;
    self.invoiceBtn.layer.cornerRadius=8;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
