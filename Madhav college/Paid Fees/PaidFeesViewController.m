
#import "PaidFeesViewController.h"
#import "Constant.h"
#import "PaidFees.h"
#import "PaidFeesTableViewCell.h"
#import "BaseViewController.h"
#import "Base.h"
#import "WebViewController.h"
#import "StudentPaidFees.h"
#import "StudentPaidFeesTableViewCell.h"
#import "PDFKBasicPDFViewer.h"
#import "PDFKDocument.h"
#import "MBProgressHUD.h"

@interface PaidFeesViewController ()
{
    BOOL shouldCellBeExpanded;
    NSInteger indexOfExpandedCell;
    NSArray *filePathsArray;
}

@end

@implementation PaidFeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shouldCellBeExpanded = NO;
    indexOfExpandedCell = -1;
    [_tableview setSeparatorColor:[UIColor clearColor]];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    
    UIActivityIndicatorView *indicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
    indicator.center=self.view.center;
    [self.view addSubview:indicator];
    indicator.tintColor=[UIColor redColor];
    indicator.backgroundColor=[UIColor lightGrayColor];
    [indicator bringSubviewToFront:self.view];
    [indicator startAnimating];
    
    _billNumberArr=[[NSMutableArray alloc]init];
    _createdDateArr=[[NSMutableArray alloc]init];
    _feesNameArr=[[NSMutableArray alloc]init];
    _payStatusArr=[[NSMutableArray alloc]init];
    _feesAmountArr=[[NSMutableArray alloc]init];
    _fineAmountArr=[[NSMutableArray alloc]init];
    _paidAmountArr=[[NSMutableArray alloc]init];
    _paidArr=[[NSMutableArray alloc]init];
    
    _downloadView.hidden=YES;

    [_billNumberArr removeAllObjects];
    [_createdDateArr removeAllObjects];
    [_feesNameArr removeAllObjects];
    [_payStatusArr removeAllObjects];
    [_feesAmountArr removeAllObjects];
    [_fineAmountArr removeAllObjects];
    [_paidAmountArr removeAllObjects];
    [_paidArr removeAllObjects];
    
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in circular==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"circular password ==%@",password);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"circular cyear ==%@",cyear);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"circular orgid ==%@",orgid);
    
    
    
    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:fees_v2]];
    
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"password":password,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id":orgid,
                                    @"cyear":cyear,
                                    @"url": urlstr,
                                    @"tag":@"paid"
                                    };
    
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response  paid fees data:%@",maindic);
            
            NSArray *ciculararr=[maindic objectForKey:@"paid_fees"];
            NSLog(@"paid_fees0000:%@",ciculararr);
            
            if(ciculararr.count==0)
            {
                
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"No data available" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alertView addAction:ok];
                [self presentViewController:alertView animated:YES completion:nil];
            }
            else {
                
                for (NSArray *coverUrlArray in ciculararr) {
                    
                    for(NSDictionary *temp in coverUrlArray)
                    {
                        NSString *str1=[temp objectForKey:@"bill_number"];
                        NSString *str2=[[temp objectForKey:@"created_date"]description];
                        NSString *str3=[[temp objectForKey:@"fees_name"]description];
                        NSString *str4=[[temp objectForKey:@"pay_status"]description];
                        NSString *str5=[[temp objectForKey:@"fees_amount"]description];
                        NSString *str6=[[temp objectForKey:@"fine_amount"]description];
                        NSString *str7=[[temp objectForKey:@"paid_amount"]description];
                        NSString *str8=[[temp objectForKey:@"due_amount"]description];
                        NSString *str9=[[temp objectForKey:@"student_id"]description];
                        NSString *str10=[[temp objectForKey:@"first_name"]description];
                        NSString *str11=[[temp objectForKey:@"last_name"]description];
                        NSString *str12=[[temp objectForKey:@"admission_no"]description];
                        NSString *str13=[[temp objectForKey:@"transaction_number"]description];
                        NSString *str14=[[temp objectForKey:@"paid_from"]description];
                        NSString *str15=[[temp objectForKey:@"total_paid_amount"]description];
                        NSString *str16=[[temp objectForKey:@"receipt_download"]description];
                        NSString *str17=[[temp objectForKey:@"fees_receipt"]description];
                        
                        NSLog(@"bill_number=%@  title=%@ publish_date=%@ publish_todate=%@",str1,str2,str3,str4);
                        if (([(NSString*)str1 isEqual: [NSNull null]])) {
                            // Showing AlertView Here
                        }else {
                            
                            PaidFees *k1=[[PaidFees alloc]init];
                            k1.billNumStr=str1;
                            k1.createdDateStr=str2;
                            k1.feesNameStr=str3;
                            k1.payStatusStr=str4;
                            k1.feesAmountStr=str5;
                            k1.fineAmountStr=str6;
                            k1.paidAmtStr=str7;
                            k1.dueAmtStr=str8;
                            k1.studentIdStr=str9;
                            k1.firstNameStr=str10;
                            k1.lastNameStr=str11;
                            k1.admissiomNumStr=str12;
                            k1.transactionNumStr=str13;
                            k1.paidFromStr=str14;
                            k1.totalPaidAmtStr=str15;
                            k1.reciecptDownloadStr=str16;
                            k1.feesRecieptStr=str17;
                            
                            
                            if ([k1.fineAmountStr containsString:@"<null>"]){
                                k1.fineAmountStr=@"0";
                            }
                            if ([k1.feesAmountStr containsString:@"<null>"]){
                                k1.feesAmountStr=@"0";
                            }
                            [self.paidArr addObject:k1];
                        }
                        [self.tableview reloadData];
                    }
                    
                }
            }
        }
        //        [_tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableview reloadData];
            
            [indicator stopAnimating];
        });
    }];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _paidArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexOfExpandedCell==indexPath.row)
    {
        PaidFeesTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        PaidFees *ktemp=[_paidArr objectAtIndex:indexPath.row];
        
        cell.billNumber.text=ktemp.billNumStr;
        cell.createdDate.text=ktemp.createdDateStr;
        cell.feesName.text=ktemp.feesNameStr;
        cell.payStatus.text=ktemp.payStatusStr;
        // cell.feesAmount.text=ktemp.feesAmountStr;
        //cell.fineAmount.text=ktemp.fineAmountStr;
        // cell.paidAmount.text=ktemp.paidAmtStr;
        cell.feesReciept.text=ktemp.feesRecieptStr;
        cell.studentId.text=ktemp.studentIdStr;
        
        NSLog(@"fees reciept url==%@",ktemp.feesRecieptStr);
        [cell.paidInvoiceBtn addTarget:self action:@selector(pdfButtonclicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.paidInvoiceBtn.tag=indexPath.row;
        [cell.paidInvoiceBtn setTag:indexPath.row];
        
        [[NSUserDefaults standardUserDefaults] setObject:ktemp.feesRecieptStr forKey:@"feeReciept"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:ktemp.billNumStr forKey:@"billNum"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:ktemp.studentIdStr forKey:@"studentId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        numberFormatter.usesGroupingSeparator = YES;
        [numberFormatter setMaximumFractionDigits:2];
        [numberFormatter setMinimumFractionDigits:2];
        NSNumber *myNum = [NSNumber numberWithDouble:[ktemp.feesAmountStr doubleValue]];
        NSString *feesAmtNew = [numberFormatter stringFromNumber:myNum];
        
        cell.feesAmount.text=feesAmtNew;
        
        
        NSNumber *myNum2 = [NSNumber numberWithDouble:[ktemp.fineAmountStr doubleValue]];
        NSString *fineAmtNew = [numberFormatter stringFromNumber:myNum2];
        
        cell.fineAmount.text=fineAmtNew;
        
        
        NSNumber *myNum3 = [NSNumber numberWithDouble:[ktemp.paidAmtStr doubleValue]];
        NSString *paidAmtNew = [numberFormatter stringFromNumber:myNum3];
        
        cell.paidAmount.text=paidAmtNew;
        
        return cell;
    }
    else
    {
        StudentPaidFeesTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
        
        StudentPaidFees *ktemp=[_paidArr objectAtIndex:indexPath.row];
        
        cell.feesName.text=ktemp.feesNameStr;
        //cell.paidAmount.text=ktemp.paidAmtStr;
        cell.selectBtn.tag=indexPath.row;
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        numberFormatter.usesGroupingSeparator = YES;
        [numberFormatter setMaximumFractionDigits:2];
        [numberFormatter setMinimumFractionDigits:2];
        NSNumber *myNum3 = [NSNumber numberWithDouble:[ktemp.paidAmtStr doubleValue]];
        NSString *paidAmtNew = [numberFormatter stringFromNumber:myNum3];
        
        cell.paidAmount.text=paidAmtNew;
        [cell.selectBtn setTag:indexPath.row];
        
        [cell.selectBtn addTarget:self action:@selector(selectBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return 0;
}
- (void)viewDidLayoutSubviews{
    NSString *language = [@"" currentLanguage];
    if ([language isEqualToString:@"hi"])
    {
        [self setBackButtonLocalize];
    }
}

- (void)setBackButtonLocalize{
    self.navigationItem.title = [@"PAID_FEES" localize];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexOfExpandedCell==indexPath.row)
    {
        
        PaidFeesTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
        NSLog(@"cell==%@",cell);
        _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
        NSLog(@"indexpath==%ld",(long)indexPath.row);
        _feesRecieptStr=cell.feesReciept.text;
        _billNumStr=cell.billNumber.text;
        _studentIdStr=cell.studentId.text;
        
        NSString *str = [instUrl stringByAppendingString:_feesRecieptStr];
        cell.feesRecieptUrl.text=str;
        
        NSLog(@"FEES rECIEPT URL %@",str);
       
        
        NSLog(@"indexpath==%ld",(long)indexPath.row);

    }
}
-(void)pdfButtonclicked:(UIButton*)sender{
    
    UIButton *btn =(UIButton *)sender;
    NSLog(@"Btn Click...........%ld",(long)btn.tag);
    indexBtn=sender.tag;
    NSLog(@"INDEX ===%ld",indexBtn);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    hud.mode = MBProgressHUDAnimationFade;
    hud.labelText = @"Downloading...";
    
    
    [self createInvoicePdfParsing];
    
}
-(void)selectBtnClicked:(UIButton *)sender{
    
    UIButton *aButton = (UIButton *)sender;
    indexOfExpandedCell = [aButton tag];
    shouldCellBeExpanded = YES;
    
    [_tableview beginUpdates];
    [_tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexOfExpandedCell inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    [_tableview endUpdates];
    [_tableview reloadData];
    NSLog(@"indexPath.row: %ld",aButton.tag);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [_tableview reloadData];
}

-(void)createInvoicePdfParsing{
    
    NSDictionary *headers = @{ @"content-type": @"application/x-www-form-urlencoded"};
    
    NSString *studentId = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"studentId"];
    NSLog(@"studentId==%@",studentId);
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"requisition branchid ==%@",branchid);
    
    NSString *billNum = [[NSUserDefaults standardUserDefaults]
                         stringForKey:@"billNum"];
    NSLog(@"billNum ==%@",billNum);
    
    NSString *mystr=[NSString stringWithFormat:@"student=%@&Branch_id=%@&bill_number=%@",studentId,branchid,billNum];
    
    NSMutableData *postData = [[NSMutableData alloc] initWithData:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://eps.eshiksa.net/esh/index.php?plugin=Fees&action=createInvoicesPdf_new"]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    //[request setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        
                                                        NSLog(@"httpResponse %@", httpResponse);
                                                        NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                        NSLog(@"text==%@",text);
                                                        NSError *er=nil;
                                                        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                                        if(er)
                                                        {
                                                        }
                                                        NSLog(@"*****responseDict:%@",responseDict);
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            //[self downloadPdf];
                                                            [self savePdfInDocumentsDirectory];
                                                            
                                                        });
                                                        
                                                        
                                                        
                                                        if([self.success isEqualToString:@"1"])
                                                        {
                                                            
                                                            NSLog(@"Data saved succesffullyy");
                                                            
                                                        }
                                                        else
                                                        {
                                                            
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
}
-(NSData *)savePdfInDocumentsDirectory
{
    PaidFees *ktemp=[_paidArr objectAtIndex:indexBtn];
    
    NSString *fileext=@".pdf";
    
    
//    NSString *branchid = [[NSUserDefaults standardUserDefaults]
//                          stringForKey:@"branchid"];
//    NSLog(@"requisition branchid ==%@",branchid);
//
//    NSString *str1=[@"/" stringByAppendingFormat:@"%@",branchid];
//    NSLog(@"***str1***%@",str1);
//
//    NSString *url3 = [NSString stringWithFormat:@"%@",[str1 stringByAppendingString:@"_"]];
    
  //  NSString *url2 = [NSString stringWithFormat:@"%@",[url3 stringByAppendingString:ktemp.billNumStr]];
    
    NSString *str3=[NSString stringWithFormat:@"%@",[downloadUrl stringByAppendingString:ktemp.billNumStr]];
    NSLog(@"***str3***%@",str3);
    
    
    _pdfPath=[str3 stringByAppendingFormat:@"%@",fileext];
    NSLog(@"***_pdfPath***  %@",_pdfPath);
    
    [[NSUserDefaults standardUserDefaults] setObject:_pdfPath forKey:@"pdfPath"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSURL *finalpdfURL = [NSURL URLWithString:[_pdfPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSLog(@"***finalURLString***%@",finalpdfURL);
    
    NSData *pdfdata = [NSData dataWithContentsOfURL:finalpdfURL];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsPath = [paths objectAtIndex:[_indxp intValue]];
    
    filePath = [documentsPath stringByAppendingPathComponent:[[_pdfPath componentsSeparatedByString:@"/"]lastObject]];
    NSLog(@"**filePath %@",filePath);
    
    [pdfdata writeToFile:filePath atomically:YES];
    
    _downloadView.hidden=NO;
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    printf("pdf file == %s",[filePath UTF8String]);
    
    
    UISaveVideoAtPathToSavedPhotosAlbum (filePath,self,@selector(pdf:didFinishSavingWithError:contextInfo:),nil);
    
    return pdfdata;
    
    
}
- (void)pdf:(NSString *)pdfPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo
{
    NSLog(@"Finished saving pdf with error: %@", error);
    
}
-(void)fetchAudioInDocumentsDirectory:(NSString*)filepath
{
    data = [NSData dataWithContentsOfFile:filePath];
    
}
- (IBAction)viewDownloadBtnClciked:(UIButton*)sender {
    
    _downloadView.hidden=YES;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:sender.tag];
    
    filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    
    NSLog(@"filePathsArray===%@ document directory=%@", filePathsArray,documentsDirectory);
    
    [self fetchAudioInDocumentsDirectory:filePath];
    
    [self performSegueWithIdentifier:@"showPDFOffline" sender:self];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"showPDFOffline"]) {
        
        PDFKBasicPDFViewer *viewer = (PDFKBasicPDFViewer *)segue.destinationViewController;
        viewer.enableBookmarks = YES;
        viewer.enableOpening = YES;
        viewer.enablePrinting = YES;
        viewer.enableSharing = YES;
        viewer.enableThumbnailSlider = YES;
        
        PDFKDocument *document = [PDFKDocument documentWithContentsOfFile:filePath password:nil];
        NSLog(@"document==%@",document);
        
        [viewer loadDocument:document];
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    int screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    switch (screenHeight) {
            
        case 1366:
        {
            if (indexOfExpandedCell==indexPath.row) {
                return 338.0f;
            }
            else{
                return 133.0f;
            }
            
        }
        case 1024:
        {
            if (indexOfExpandedCell==indexPath.row) {
                return 338.0f;
            }
            else{
                return 133.0f;
            }
            
        }
        default:
        {
            if (indexOfExpandedCell==indexPath.row) {
                return 184.0f;
            }
            else{
                return 64.0f;
            }
            
            break;
        }
    }
    return 184.0f;
}


@end

