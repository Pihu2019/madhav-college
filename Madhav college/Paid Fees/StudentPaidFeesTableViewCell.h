

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StudentPaidFeesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *feesName;
@property (weak, nonatomic) IBOutlet UILabel *paidAmount;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;

@end

NS_ASSUME_NONNULL_END
