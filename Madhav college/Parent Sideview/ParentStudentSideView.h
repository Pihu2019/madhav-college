//
//  ParentStudentSideView.h
//  Eshiksa
//
//  Created by Punit on 03/10/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParentStudentSideView : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *studentname;
@property (weak, nonatomic) IBOutlet UIView *profileView;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *dashboardTxt;
@property (weak, nonatomic) IBOutlet UILabel *courseTxt;
@property (weak, nonatomic) IBOutlet UILabel *settingTxt;
@property (weak, nonatomic) IBOutlet UILabel *selectStudentTxt;
@property (weak, nonatomic) IBOutlet UILabel *logoutTxt;

@end

NS_ASSUME_NONNULL_END
