
#import "ParentStudentSideView.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "BaseViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Base.h"
@interface ParentStudentSideView ()

@end

@implementation ParentStudentSideView
@synthesize dashboardTxt;
@synthesize courseTxt;
@synthesize settingTxt;
@synthesize logoutTxt;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *admissionNum1 = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"admissionNum1"];
    self.studentname.text=admissionNum1;
    
    self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
    self.profileImg.clipsToBounds = YES;
    
    
    self.profileView.layer.cornerRadius = self.profileView.frame.size.width / 2;
    self.profileView.clipsToBounds = YES;
 
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];
    
    dashboardTxt.text = [@"DASHBOARD" localize];
    courseTxt.text=[@"COURSE" localize];
   
    settingTxt.text=[@"SETTINGS" localize];

_selectStudentTxt.text=[@"SELECT_STUDENT" localize];
    logoutTxt.text=[@"LOGOUT" localize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage:) name:@"notificationName" object:nil];
}

-(void)changeLanguage:(NSNotification*)notification
{
    dashboardTxt.text = [@"DASHBOARD" localize];
    courseTxt.text=[@"COURSE" localize];
    
    settingTxt.text=[@"SETTINGS" localize];
   
    _selectStudentTxt.text=[@"SELECT_STUDENT" localize];
    logoutTxt.text=[@"LOGOUT" localize];
}
- (IBAction)logoutBtnClicked:(id)sender {

    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
