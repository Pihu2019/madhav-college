
#import "StudentParentViewController.h"
#import "BaseViewController.h"
#import "Constant.h"
#import "Base.h"
@interface StudentParentViewController ()

@end

@implementation StudentParentViewController
@synthesize profiletxt;
@synthesize circulartext;
@synthesize gallerytxt;
@synthesize payfeestxt;
@synthesize paidfeestxt;
@synthesize settingstxt;
@synthesize poweredBy;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.profileBtn.layer.cornerRadius=6.0f;
    self.galleryBtn.layer.cornerRadius=6.0f;
    self.paidBtn.layer.cornerRadius=6.0f;
    self.circularBtn.layer.cornerRadius=6.0f;
    self.payfeeBtn.layer.cornerRadius=6.0f;
    self.settingBtn.layer.cornerRadius=6.0f;
    
    self.admissionNum.text=_admisnNum;
    self.firstNamelbl.text=_firstName;
    self.lastnamelbl.text=_lastName;
    self.branchIdlbl.text=_branchId;
    self.emaillbl.text=_emailId;
    self.addresslbl.text=_address;
    self.orgIdlbl.text=_orgId;
    self.mobilelbl.text=_mobile;
    self.opyearlbl.text=_opyear;
    self.picIdlbl.text=_picId;
    self.studentIdlbl.text=_studentId;
    self.transportLicencelbl.text=_transportLicence;
    
    NSLog(@"ADMISSIONNNUM%@",_admisnNum);
    [[NSUserDefaults standardUserDefaults] setObject:self.admissionNum.text forKey:@"admissionNum1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:  self.mobilelbl.text forKey:@"mobile1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject: self.addresslbl.text forKey:@"address1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setObject: self.studentIdlbl.text forKey:@"studentID1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.picIdlbl.text forKey:@"picId1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
        [[NSUserDefaults standardUserDefaults] setObject:self.branchIdlbl.text forKey:@"branchid1"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
        [[NSUserDefaults standardUserDefaults] setObject: self.orgIdlbl.text forKey:@"orgid1"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
        [[NSUserDefaults standardUserDefaults] setObject: self.firstNamelbl.text forKey:@"username1"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject: self.lastnamelbl.text forKey:@"lastname1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
        [[NSUserDefaults standardUserDefaults] setObject: self.opyearlbl.text forKey:@"cyear1"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject: self.emaillbl.text forKey:@"email1"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    [self pushNotificationParsing]; //uncomment for push notification while running in device
}
-(void)pushNotificationParsing{
    
    NSString *token_id = [[NSUserDefaults standardUserDefaults]stringForKey:@"fcmToken"];
    NSLog(@"*****token_id ==%@",token_id);
    NSString *deviceidStr = [[NSUserDefaults standardUserDefaults]stringForKey:@"deviceToken"];
    NSLog(@"*****deviceId str ==%@",deviceidStr);
    NSString *username = [[NSUserDefaults standardUserDefaults]stringForKey:@"username"];
    NSLog(@"*****username ==%@",username);
    
    NSString *mainstr1=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:push_notifications]];
    
    NSDictionary *parameterDict =@{
                                   @"tag":@"insert_token",
                                   @"token_id":token_id,
                                   @"device_id":deviceidStr,
                                   @"dbname":dbname,
                                   @"user_id":username
                                   };
    NSLog(@"*****parameter dic==%@",parameterDict);
    
    [Constant executequery:mainstr1 strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response :%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[[maindic objectForKey:@"success"]stringValue];
            _error=[[maindic objectForKey:@"error"]stringValue];
            
            if([self.success isEqualToString:@"1"])
            {
                NSLog(@"SUCCESS................");
                
            }
            else
            {
                NSLog(@"ERROR............");
            }
            
        }
    }];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];
    
    profiletxt.text = [@"MY_PROFILE" localize];
    circulartext.text=[@"CIRCULAR" localize];
    payfeestxt.text = [@"PAY_FEES" localize];
    paidfeestxt.text=[@"PAID_FEES" localize];
    gallerytxt.text=[@"GALLERY" localize];
    settingstxt.text=[@"SETTINGS" localize];
    poweredBy.text=[@"POWERED_BY" localize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage:) name:@"notificationName" object:nil];
    
    [super viewDidAppear:(BOOL)animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}

-(void)changeLanguage:(NSNotification*)notification
{
    profiletxt.text = [@"MY_PROFILE" localize];
    circulartext.text=[@"CIRCULAR" localize];
    payfeestxt.text = [@"PAY_FEES" localize];
    paidfeestxt.text=[@"PAID_FEES" localize];
    gallerytxt.text=[@"GALLERY" localize];
    settingstxt.text=[@"SETTINGS" localize];
    poweredBy.text=[@"POWERED_BY" localize];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
