
#import "ChildListViewController.h"
#import "ChildTableViewCell.h"
#import "Base.h"
#import "Constant.h"
#import "StudentParentViewController.h"
@interface ChildListViewController ()

@end

@implementation ChildListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.tableview setSeparatorColor:[UIColor clearColor]];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    
    
    _firstNameArr=[[NSMutableArray alloc]init];
    _admissionNumArr=[[NSMutableArray alloc]init];
    _childListArr=[[NSMutableArray alloc]init];
  

    [self requestData];
}
-(void)requestData{

    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in circular==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);

    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"circular password ==%@",password);
    
    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:auth]];
    
    NSDictionary *parameterDict = @{
                                    @"tag":@"login",
                                    @"Username":username,
                                    @"Password":password,
                                    @"dbname":dbname,
                                    @"branch_id":branchid,
                                    @"instUrl":instUrl
                                    };
    
    NSLog(@"Parameter dict%@",parameterDict);
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response data:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[maindic objectForKey:@"success"];
            _error=[maindic objectForKey:@"error"];
            _user=[maindic objectForKey:@"user"];
            _childUser=[maindic objectForKey:@"ChildUser"];
          
            
            NSLog(@"Tag===%@ user=%@ cHILDuser=%@",_tag,_user,_childUser);
            
            
            NSArray *ciculararr=[maindic objectForKey:@"ChildUser"];
            
            
            NSLog(@"ChildUser:%@",ciculararr);
            
            if(ciculararr.count==0)
            {
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"Currently there is no child." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
                
                
                [alertView addAction:ok];
                
                [self presentViewController:alertView animated:YES completion:nil];
                
            }
            else {
                
                
                for(NSDictionary *temp in ciculararr)
                {
                    NSString *str1=[[temp objectForKey:@"student_id"]description];
                    NSString *str2=[[temp objectForKey:@"first_name"]description];
                    NSString *str3=[[temp objectForKey:@"last_name"]description];
                    NSString *str4=[[temp objectForKey:@"admission_no"]description];
                    NSString *str5=[[temp objectForKey:@"mobile"]description];
                    NSString *str6=[[temp objectForKey:@"email"]description];
                    NSString *str7=[[temp objectForKey:@"address"]description];
                    NSString *str8=[[temp objectForKey:@"Branch_id"]description];
                    NSString *str9=[[temp objectForKey:@"opyear"]description];
                    NSString *str10=[[temp objectForKey:@"org_id"]description];
                    NSString *str11=[[temp objectForKey:@"pic_id"]description];
                    NSString *str12=[[temp objectForKey:@"transport_license"]description];
                    
                    NSLog(@"from_date=%@  to_date=%@ created_date=%@ status=%@",str1,str2,str3,str4);
                    
                    
                    [_childListArr addObject:temp];
                    NSLog(@"_gatepassArr ARRAYY%@",_childListArr);
                }
                [_tableview reloadData];
            }
        }
        [_tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableview reloadData];
            
            
        });
    }];
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _childListArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChildTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSMutableDictionary *ktemp=[_childListArr objectAtIndex:indexPath.row];
    cell.admissionNum.text=[[ktemp objectForKey:@"admission_no"]description];
    cell.firstName.text=[[ktemp objectForKey:@"first_name"]description];
    cell.lastname.text=[[ktemp objectForKey:@"last_name"]description];
    cell.studentId.text=[[ktemp objectForKey:@"student_id"]description];
    cell.mobile.text=[[ktemp objectForKey:@"mobile"]description];
    cell.email.text=[[ktemp objectForKey:@"email"]description];
    cell.address.text=[[ktemp objectForKey:@"address"]description];
    cell.branchId.text=[[ktemp objectForKey:@"Branch_id"]description];
    cell.opyear.text=[[ktemp objectForKey:@"opyear"]description];
    cell.picId.text=[[ktemp objectForKey:@"pic_id"]description];
    cell.orgId.text=[[ktemp objectForKey:@"org_id"]description];
    cell.transportLicence.text=[[ktemp objectForKey:@"transport_license"]description];
    
    
     return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ChildTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
    _admissionNumStr=cell.admissionNum.text;
    _firstName=cell.firstName.text;
    _lastName=cell.lastname.text;
    _studentIdStr=cell.studentId.text;
    _mobileStr=cell.mobile.text;
    _emailStr=cell.email.text;
    _addressStr=cell.address.text;
    _branchIdStr=cell.branchId.text;
    _opyearStr=cell.opyear.text;
    _picIdStr=cell.picId.text;
    _orgIdStr=cell.orgId.text;
    _transportLicenceStr=cell.transportLicence.text;
    
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    NSLog(@"indexpath==%ld",(long)indexPath.row);
    
    [self performSegueWithIdentifier:@"studentDetails"
                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"studentDetails"])
    {
        
        StudentParentViewController *kvc = [segue destinationViewController];
        
        kvc.admisnNum=_admissionNumStr;
        kvc.firstName=_firstName;
        kvc.lastName=_lastName;
        kvc.studentId=_studentIdStr;
        kvc.mobile=_mobileStr;
        kvc.emailId=_emailStr;
        kvc.address=_addressStr;
        kvc.branchId=_branchIdStr;
        kvc.opyear=_opyearStr;
        kvc.picId=_picIdStr;
        kvc.orgId=_orgIdStr;
        kvc.transportLicence=_transportLicenceStr;
        kvc.indxpath=_indxp;
        
        NSLog(@"indexpath in prepare for segue==%@",_admissionNumStr);
    }
}


@end
