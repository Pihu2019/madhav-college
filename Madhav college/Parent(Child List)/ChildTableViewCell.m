
#import "ChildTableViewCell.h"

@implementation ChildTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_view.bounds];
    _view.layer.masksToBounds = NO;
   _view.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _view.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _view.layer.shadowOpacity = 0.5f;
    _view.layer.shadowPath = shadowPath.CGPath;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
