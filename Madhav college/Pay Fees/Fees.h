

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Fees : NSObject

@property (nonatomic,strong) NSString *fees_nameStr,*fees_amountStr,*due_dateStr,*due_amountStr,*paid_amountStr,*total_concession_amountStr,*head_fine_amountStr,*courseIdStr,*batchIdStr,*departmentIdStr,*sessionIdStr,*studentIdStr,*feesIdStr,*onlinePayDiscStr,*totalConcessionAmtStr,*totalCalculatedAmountStr;

@end

NS_ASSUME_NONNULL_END
