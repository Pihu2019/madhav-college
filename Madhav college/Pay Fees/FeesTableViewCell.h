

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FeesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *feesName;
@property (weak, nonatomic) IBOutlet UILabel *dueAmount;
@property (weak, nonatomic) IBOutlet UIButton *selectBtn;


@end

NS_ASSUME_NONNULL_END
