

#import <UIKit/UIKit.h>

@interface PayFeesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong) NSMutableArray *feesNameArr,*feesAmountArr,*dueDateArr,*dueAmountArr,*paidAmountArr,*totalConcessionAmountArr,*headFineAmountArr,*payArr;

@property(nonatomic,retain)NSString *indxp,*feesAmtStr,*courseIdStr,*batchIdStr,*departmentIdStr,*sessionIdStr,*studentIdStr,*dueAmtStr,*totalConcessionAmtStr,*headFineAmountStr,*feesIdStr,*feesBaseId,*feesShortId;
//@property (strong, nonatomic) IBOutlet UITextView *noSchedule;

@end
