

#import "PayNowViewController.h"
#import "Constant.h"
#import "PayNow.h"
#import "WebViewController.h"
#import "Base.h"
@interface PayNowViewController ()
{
    NSString *commissionAmount,*netAmt,*totalAmt;
}
@end

@implementation PayNowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.proceedBtn.layer.masksToBounds=YES;
    self.proceedBtn.layer.cornerRadius=16;
    
    _batchid.text=_batchIdStr;
    _sessionId.text=_sessionIdStr;
    _departmentId.text=_departmentIdStr;
    _courseid.text=_courseIdStr;
    _studentId.text=_studentIdStr;
    //_payableAmount.text=_feesAmtStr;
  
    double result = [_dueFeesStr doubleValue] - [_totalConcessionAmtStr doubleValue] + [_headFineAmtStr doubleValue];
    totalAmt =[NSString stringWithFormat:@"%.02f", result];
    NSLog(@"****totalAmt  %@",totalAmt);
    NSLog(@"****_dueFeesStr  %@",_dueFeesStr);

    _netAmount.text=totalAmt;
    _payableAmount.text=totalAmt;
    
     NSLog(@"SESSION ID=%@ batch id=%@ department id=%@ courseIdStr=%@ studentIdStr=%@ feesAmtStr=%@",_sessionIdStr,_batchIdStr,_departmentIdStr,_courseIdStr,_studentIdStr,_feesAmtStr);
    
    [_proceedBtn setHidden:YES];
    //[self excessAmountDataParsing];
   
}

- (IBAction)creditCardBtnClicked:(id)sender {
    NSLog(@"CREDIT CARD BTN CLICKED...");
    [self pgParamsDataParsing];

    _cardtypeStr=@"creditcard";
    [_creditCardBtn setImage:[UIImage imageNamed:@"clicked64.png"]forState:UIControlStateNormal];
    [_debitCardBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    [_netBankingBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
     [_upiBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    
    NSString *pgccbanking = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"pgccbanking"];
    NSLog(@"pgccbanking==%@",pgccbanking);
    self.trasactionCharges.text=pgccbanking;
    
    NSString *newString = [pgccbanking substringToIndex:[pgccbanking length]-1];
    
    NSLog(@"NewString**=%@",newString);
    
    double str1 = [_dueFeesStr  doubleValue];
    double str2 = [newString doubleValue];
    
    double str3=str1*str2;
    NSLog(@"sTRING 3==%f",str3);
    
    double str4 = str3 / 100;
    NSLog(@"sTRING 4==%f",str4);
    
    commissionAmount =[NSString stringWithFormat:@"%f", str4];
    NSLog(@"commissionAmount 4==%@",commissionAmount);
    
    double sum =[totalAmt doubleValue] + [commissionAmount doubleValue];

    self.netAmount.text =[NSString stringWithFormat:@"%.02f", sum];

    NSLog(@"self.netAmount.text==%@",self.netAmount.text);
    
    [[NSUserDefaults standardUserDefaults] setObject: self.netAmount.text forKey:@"netAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
     [_proceedBtn setHidden:NO];
}

- (IBAction)debitCardBtnClicked:(id)sender {
    NSLog(@"DEBIT CARD BTN CLICKED...");
    [self pgParamsDataParsing];
    _cardtypeStr=@"debitcard";
    [_debitCardBtn setImage:[UIImage imageNamed:@"clicked64.png"] forState:UIControlStateNormal];
    [_creditCardBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    [_netBankingBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
     [_upiBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    
    NSString *pgdcbanking = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"pgdcbanking"];
    NSLog(@"pgdcbanking==%@",pgdcbanking);
    self.trasactionCharges.text=pgdcbanking;
    
    NSString *newString = [pgdcbanking substringToIndex:[pgdcbanking length]-1];
    
    NSLog(@"NewString**=%@",newString);
    
    double str1 = [_dueFeesStr  doubleValue];
    double str2 = [newString doubleValue];
    
    double str3=str1*str2;
    NSLog(@"sTRING 3==%f",str3);
    
    double str4 = str3 / 100;
    NSLog(@"sTRING 4==%f",str4);
    
    commissionAmount =[NSString stringWithFormat:@"%f", str4];
    NSLog(@"commissionAmount 4==%@",commissionAmount);
    
    double sum =[totalAmt doubleValue] + [commissionAmount doubleValue];
    self.netAmount.text =[NSString stringWithFormat:@"%.02f", sum];
    
       NSLog(@"self.netAmount.text==%@",self.netAmount.text);
 
    [[NSUserDefaults standardUserDefaults] setObject:self.netAmount.text forKey:@"netAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
     [_proceedBtn setHidden:NO];

}
- (IBAction)netBankingBtnClicked:(id)sender {
       NSLog(@"NET BANKING  BTN CLICKED...");
    [self pgParamsDataParsing];
    _cardtypeStr=@"netbanking";
    
    [_netBankingBtn setImage:[UIImage imageNamed:@"clicked64.png"] forState:UIControlStateNormal];
    [_creditCardBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    [_debitCardBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
     [_upiBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];

    NSString *pgnetbanking = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"pgnetbanking"];
    NSLog(@"pgnetbanking==%@",pgnetbanking);
    self.trasactionCharges.text=pgnetbanking;
    commissionAmount=pgnetbanking;
    double result=[totalAmt doubleValue]+[pgnetbanking doubleValue];
    netAmt = [NSString stringWithFormat:@"%.1f", result];
                NSLog(@"net amount==  %@",netAmt);
    self.netAmount.text=netAmt;
    
    [[NSUserDefaults standardUserDefaults] setObject: self.netAmount.text forKey:@"netAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
     [_proceedBtn setHidden:NO];
}

- (IBAction)upiBtnClicked:(id)sender {
    
    NSLog(@"upi BTN CLICKED...");
    [self pgParamsDataParsing];
    _cardtypeStr=@"upi";
    
    [_upiBtn setImage:[UIImage imageNamed:@"clicked64.png"] forState:UIControlStateNormal];
    [_creditCardBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    [_debitCardBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    [_netBankingBtn setImage:[UIImage imageNamed:@"unclicked64.png"]forState:UIControlStateNormal];
    
    
    NSString *pgUPI = [[NSUserDefaults standardUserDefaults]
                             stringForKey:@"pgUPI"];
    NSLog(@"pgUPI==%@",pgUPI);
    self.trasactionCharges.text=pgUPI;
     commissionAmount=pgUPI;
    double result=[totalAmt doubleValue]+[pgUPI doubleValue];
    netAmt = [NSString stringWithFormat:@"%.1f", result];
    NSLog(@"net amount UPI==  %@",netAmt);
    self.netAmount.text=netAmt;
    
    [[NSUserDefaults standardUserDefaults] setObject: self.netAmount.text forKey:@"netAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     [_proceedBtn setHidden:NO];
}

- (IBAction)proceedBtnClicked:(UIButton *)sender {
    
      NSLog(@"proceed BTN CLICKED...");
    
   //[self savePgParamsDataParsing];
    
    [self performSegueWithIdentifier:@"showPayInfo" sender:nil];
    
}
-(void)pgParamsDataParsing{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in requisition==%@",groupname);
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"requisition branchid ==%@",branchid);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"requisition orgid ==%@",orgid);
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"requisition cyear ==%@",cyear);
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"requisition password ==%@",password);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"profile username ==%@",username);
    
    NSString *str=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:pgParams]];
  
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"instUrl":instUrl,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id": orgid,
                                    @"cyear": cyear,
                                    @"url":str,
                                    @"tag": @"fees",
                                    @"password": password
                                 
                                    };
    
    NSLog(@"pg data parameter dict==%@",parameterDict);
    
    [Constant executequery:str  strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response pgParams:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[[maindic objectForKey:@"success"]stringValue];
            _error=[[maindic objectForKey:@"error"]stringValue];
            _merchantId=[maindic objectForKey:@"merchantId"];
            _pgURL=[maindic objectForKey:@"pgURL"];
            _pgAction=[maindic objectForKey:@"pgAction"];
            _instId=[maindic objectForKey:@"instId"];
            _instName=[maindic objectForKey:@"instname"];
            _pgName=[maindic objectForKey:@"pgName"];
            _encryptKey=[maindic objectForKey:@"encryptKey"];
            _pgCCComission=[maindic objectForKey:@"pgCCComission"];
            _pgDCComission=[maindic objectForKey:@"pgDCComission"];
            _pgNBComissionStr=[maindic objectForKey:@"pgNBComission"];
            _pgUrlStr=[maindic objectForKey:@"appPgURL"];
            _pgUPComission=[maindic objectForKey:@"pgUPComission"];
            _upiEnable=[maindic objectForKey:@"upi_enable"];
            
            
            
            NSLog(@"tag==%@& success=%@  _merchantId =%@ pgCCComission=%@ debit pgDCComission=%@ netbanking pgNBComission=%@ pgUPComission=%@",_tag,_success,_merchantId,_pgCCComission,_pgDCComission,_pgNBComissionStr,_pgUPComission);
            
            [[NSUserDefaults standardUserDefaults] setObject:_encryptKey forKey:@"encryptkey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
         
            [[NSUserDefaults standardUserDefaults] setObject:_pgName forKey:@"pgname"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:_merchantId forKey:@"merchantid"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //for Branchid:565
            
            [[NSUserDefaults standardUserDefaults] setObject:_pgCCComission forKey:@"pgccbanking"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:_pgDCComission forKey:@"pgdcbanking"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:_pgNBComissionStr forKey:@"pgnetbanking"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:_pgUPComission forKey:@"pgUPI"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
         /* //for branchID=1
          [[NSUserDefaults standardUserDefaults] setObject:_pgNBComissionStr forKey:@"pgnetbanking"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *amountAdded = [[NSUserDefaults standardUserDefaults]
                                     stringForKey:@"amountadded"];
            NSLog(@"amountAdded ==%@",amountAdded);
            
            NSString *amountadded2 = [[NSUserDefaults standardUserDefaults]
                                      stringForKey:@"amountadded2"];
            NSLog(@"amountadded2 ==%@",amountadded2);
            
            int result=[amountAdded intValue]+[_pgNBComissionStr intValue]-[amountadded2 intValue];
            totalAmountPaid = [NSString stringWithFormat:@"%d", result];
            NSLog(@"totalAmountPaid==  %@",totalAmountPaid);
            
            [[NSUserDefaults standardUserDefaults] setObject:totalAmountPaid forKey:@"totalAmountPaid"];
            [[NSUserDefaults standardUserDefaults] synchronize];
       
            
            // $    check[0]['FEES_AMOUNT']+$check[0]['fine_amount']+$check[0]['pg_commission']-($check[0]['concession_amount']+$check[0]['discount_amount']);
          */
            if([self.success isEqualToString:@"1"])
            {
                NSLog(@"Data saved succesffullyy");
                [self savePgParamsDataParsing];
                
            }
            else
            {
                 NSLog(@"Something went wrong..");
         }
            
        }
    }];
}

-(void)savePgParamsDataParsing{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in requisition==%@",groupname);
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"requisition branchid ==%@",branchid);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"requisition orgid ==%@",orgid);
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"requisition cyear ==%@",cyear);
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"requisition password ==%@",password);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"profile username ==%@",username);
    
    NSString *pgname = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"pgname"];
    NSLog(@"pgname==%@",pgname);
    
    NSString *encryptkey = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"encryptkey"];
    NSLog(@"encryptKey==%@",encryptkey);
    
    NSString *merchantid = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"merchantid"];
    NSLog(@"merchantId==%@",merchantid);
    
 //for BranchID::565 feesId_studentId_courseId_batchId_totalAmount_commAmt_departmentId_sessionID_username_userExcessAmt
    _udf1 = [NSString stringWithFormat: @"%@%s%@%s%@%s%@%s%@%s%@%s%@%s%@%s%@%s%d", _feesIdStr,"_",_studentIdStr,"_",_courseIdStr,"_",_batchIdStr,"_",totalAmt,"_",commissionAmount,"_",_departmentIdStr,"_",_sessionIdStr,"_",username,"_",0];
 
 NSLog(@"UDF1 DATA==%@",_udf1);

 //   UDF1 DATA==52858 _ 317564 _ 3832 _ 5289 _ 3244 _ 650 _ 0 _ 0 _ JPS2391 _ 0
 
 /*  //for branchId:1
    _udf1 = [NSString stringWithFormat: @"%@ %s %@ %s %@ %s %@ %s %@ %s %@ %s %d %s %d %s %@ %s %d", feesId,"_",_studentIdStr,"_",_courseIdStr,"_",_batchIdStr,"_",totalAmountPaid,"_",pgnetbanking,"_",0,"_",0,"_",username,"_",0];
    
    NSLog(@"UDF1 DATA==%@",_udf1);
    
    //0 _ 261740 _ 2908 _ 3897 _ 1200 _ 0 _  _  _ JASL3111 _ 1200
    
     
     "pay_method":"debitcard",
     "udf1":"8585_121277_1381_1883_1200_0.00_0_0_RAJV129121277_0",
     feesId_studentId_courseId_batchId_totalAmount_charge_departmentId_sessionID_username_userExcessAmt
     0 _ 261742 _ 2908 _ 3897 _ 2600 _ 0 _ 0 _ 0 _ ARVI311261742 _ 2600

     $scope.studentInfo.fees_id + "_" + $scope.studentInfo.student_id + "_" + $scope.studentInfo.course_id + "_" + $scope.studentInfo.batch_id + "_" + amountObject.totalamt + "_" + $scope.charge + "_" + $scope.studentInfo.department_id + "_" + $scope.studentInfo.session_id + "_" + RestService.getUsername() + "_" + amountObject.user_excess_amount;
     
     */
    
    
    /*
     {
     "Branch_id":"129",
     "amount":1200,
     "charge":"0.00",
     "dbname":"erpeshik_esh_slave",
     "groupname":"Student",
     "instUrl":"http://erp.eshiksa.net/eps",
     "key":"160",
     "password":"12345",
     "pay_method":"debitcard",
     "pgName":"atom",
     "salt":"Test@123",
     "surl":"http://shop.eshiksa.com/appAPI_v2/",
     "tag":"savePGData",
     "udf1":"8585_121277_1381_1883_1200_0.00_0_0_RAJV129121277_0",
     "udf2":"erpeshik_esh_slave",
     "udf3":"129",
     "url":"http://shop.eshiksa.com/appAPI_v2/savePGData_v2.php",
     "user_name":"17DUMM121950"
     }
     */
    
    
      NSString *str=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:savePGData_v2]];

    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"instUrl":instUrl,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id": orgid,
                                    @"cyear": cyear,
                                    @"url":str,
                                    @"tag":@"savePGData",
                                    @"password": password,
                                    @"amount":totalAmt,
                                    @"charge":commissionAmount,
                                    @"key":merchantid,
                                    @"pay_method":_cardtypeStr,
                                    @"pgName":pgname,
                                    @"salt":encryptkey,
                                    @"surl":mainUrl,
                                    @"udf1":_udf1,
                                    @"udf2":dbname,
                                    @"udf3":branchid
                                    };
    
    NSLog(@"parameter dict==%@",parameterDict);

    [Constant executequery:str  strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response data in save pg...:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[maindic objectForKey:@"success"];
            _error=[maindic objectForKey:@"error"];
            _tempIdStr=[maindic objectForKey:@"tempId"];
            _txnIdStr=[maindic objectForKey:@"txnid"];
            _concessionStatusStr=[maindic objectForKey:@"concession_status"];

            NSLog(@"tag==%@& success=%@  _txnIdStr =%@  _tempIdStr=%@  _concessionStatusStr=%@",_tag,_success,_txnIdStr,_tempIdStr,_concessionStatusStr);
            
            NSString *txnid=_txnIdStr;
            NSLog(@"IN API txnid---%@",txnid);
            
            [[NSUserDefaults standardUserDefaults] setObject:_txnIdStr forKey:@"transactionId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
             NSLog(@"***transactionId***** = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"transactionId"]);
        
            
//           if([self.success isEqualToString:@"1"])
//            {
//                NSLog(@"success.....");
//            }
//            else
//            {
//                NSLog(@"failure.....");
//            }
        }
    }];
}
-(void)excessAmountDataParsing{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in requisition==%@",groupname);
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"requisition branchid ==%@",branchid);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"requisition orgid ==%@",orgid);
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"requisition cyear ==%@",cyear);
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"requisition password ==%@",password);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"profile username ==%@",username);

    NSString *str=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:excess_amount]];

    
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"instUrl":instUrl,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id": orgid,
                                    @"cyear": cyear,
                                    @"url":str,
                                    @"tag": @"excess_amount",
                                    @"password": password
                                    
                                    };
    
    NSLog(@"parameter dict==%@",parameterDict);
    
    [Constant executequery:str  strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"excess data response data:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[maindic objectForKey:@"success"];
            _error=[maindic objectForKey:@"error"];
            
            NSLog(@"tag==%@& success=%@",_tag,_success);
            
            [[NSOperationQueue mainQueue]addOperationWithBlock:^{
                
            }];
            
            NSDictionary *dic=[maindic objectForKey:@"excess_data"];
            
           NSLog(@"excess data:%@",dic);
            
                
            PayNow *p=[[PayNow alloc]init];
            p.feesid=[dic objectForKey:@"fees_id"];
            p.feesName=[dic objectForKey:@"fees_name"];
            p.feesAmount=[dic objectForKey:@"fees_amount"];
            p.excessAmount=[dic objectForKey:@"excess_amount"];
            
            NSLog(@"feesid:::%@ feesName :%@ excessAmount id:%@",[dic objectForKey:@"fees_id"],p.feesName,p.excessAmount);
                if(p.feesid==(NSString *) [NSNull null])
                {
                    p.feesid=@"0";
                }
                if(p.feesAmount==(NSString *) [NSNull null])
                {
                    p.feesAmount=@"0";
                }
                if(p.excessAmount==(NSString *) [NSNull null])
                {
                    p.excessAmount=@"0";
                }
                if(p.feesName==(NSString *) [NSNull null])
                {
                    p.feesName=@"-";
                }
            
            [[NSUserDefaults standardUserDefaults] setObject:p.excessAmount forKey:@"excessAmount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            

        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    WebViewController *wvc=[segue destinationViewController];
    if ([segue.identifier isEqualToString:@"showPayInfo"]) {
        
        NSString *txnid = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"transactionId"];
        NSLog(@"***transactionId ==%@",txnid);
        
        NSString *branchid = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"branchid"];
        NSLog(@"***branchid ==%@",branchid);
        
        NSString *netAmount = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"netAmount"];
        NSLog(@"netAmount==%@",netAmount);
       
       NSString *myst=[NSString stringWithFormat:@"plugin=payment&action=pay&txnid=%@&branch_id=%@&amount=%@",txnid,branchid,netAmount];
        
    
        
        NSLog(@"my string commentsss=%@",myst);
        
       // NSString *str = [@"http://eps.eshiksa.net/esh/index.php?" stringByAppendingString:myst];//LIVE URL
       // NSString *str = [@"http://erp.eshiksa.net/edemo_fees/esh/index.php?" stringByAppendingString:myst];*///commented for edemo maldivess
        
       // NSString *str = [pgUrl stringByAppendingString:myst];
        
       
        NSString *str = [@"http://eps.eshiksa.net/esh/index.php?" stringByAppendingString:myst];
        
        // NSString *str = [@"http://erp.eshiksa.net/eps_trunk/eps/esh/index.php?" stringByAppendingString:myst];
        
         wvc.myURL=str;
        
        NSLog(@"*******full str=%@",str);
        
       // http://eps.eshiksa.net/esh/index.php?plugin=payment&action=pay&txnid=esha_5c6e9f6bd06f91550753643&branch_id=554&amount=6613.13
        
        //http://erp.eshiksa.net/eps_trunk/eps/esh/index.php?plugin=payment&action=pay&txnid=esha_833e7f7992ada00d6e45&branch_id=565&amount=3568.750000
       
        //http://erp.eshiksa.net/edemo_maldives/esh/index.php?plugin=pay&action=index&txnid=esha_ddf76d216cf18a75fff3

        //wvc.myURL=@"http://erp.eshiksa.net/edemo_fees/esh/index.php?plugin=payment&action=pay&txnid=esh_5af40add0cf3f&branch_id=65&amount=2400";
       
        //http://eps.eshiksa.net/esh/index.php?plugin=payment&action=pay&student_id=200978&txnid=esh_5b026f5895ba6&amount=100&branch_id=312 //new db working url
    }
}

@end
