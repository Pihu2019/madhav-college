

#import "ParentChildProfileVC.h"
#import <Foundation/Foundation.h>
#import "Constant.h"
#import "BaseViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Base.h"
@interface ParentChildProfileVC ()
{
    BOOL isSelected;
}
@end

@implementation ParentChildProfileVC
@synthesize studentIdtext;
@synthesize sectiontxt;
@synthesize coursetxt;
@synthesize dateofbirthtxt;
@synthesize addresstxt;
@synthesize emailtxt;
@synthesize batchtxt;
@synthesize mobiletxt;
@synthesize aboutTxt;
@synthesize contactTxt;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_downBtn setTitle:@"Contact" forState:UIControlStateNormal];
    [_downBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    isSelected=YES;
    _contactView.hidden=YES;
    _aboutView.hidden=NO;
    self.profilepicImg.layer.cornerRadius = self.profilepicImg.frame.size.width / 2;
    self.profilepicImg.clipsToBounds = YES;
    //self.profilepicImg.layer.borderWidth = 1.0f;
    //self.profilepicImg.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.profileview.layer.cornerRadius = self.profileview.frame.size.width / 2;
    self.profileview.clipsToBounds = YES;
   // self.profileview.layer.borderWidth = 1.0f;
   // self.profileview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = _downBtn.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:(18.0/225.0) green:(132.0/225.0) blue:(204.0/255.0)alpha:1.0].CGColor, (id)[UIColor colorWithRed:(185.0/225.0) green:(156.0/225.0) blue:(231.0/255.0)alpha:1.0].CGColor,(id)[UIColor colorWithRed:(202.0/225.0) green:(99.0/225.0) blue:(210.0/255.0)alpha:1.0].CGColor];
    
    [_downBtn.layer insertSublayer:gradient atIndex:0];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_downBtn.bounds
                                                   byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft)
                                                         cornerRadii:CGSizeMake(6.0, 6.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = _downBtn.bounds;
    maskLayer.path = maskPath.CGPath;
    // Set the newly created shapelayer as the mask for the image view's layer
    _downBtn.layer.mask = maskLayer;
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:_upBtn.bounds
                                                    byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight)
                                                          cornerRadii:CGSizeMake(6.0, 6.0)];
    CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
    maskLayer1.frame = _upBtn.bounds;
    maskLayer1.path = maskPath1.CGPath;
    _upBtn.layer.mask = maskLayer1;
 
    
    NSString *cyear1 = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"cyear1"];

    
    NSString *admissionNum1 = [[NSUserDefaults standardUserDefaults]
                         stringForKey:@"admissionNum1"];
    
    NSString *branchid1 = [[NSUserDefaults standardUserDefaults]
                               stringForKey:@"branchid1"];
    NSString *orgid1 = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"orgid1"];

    
    NSString *mainstr1=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:profile]];
    
    
    NSDictionary *parameterDict =
    @{ @"groupname":@"Student",
       @"username": admissionNum1,
       @"dbname":dbname,
       @"Branch_id": branchid1,
       @"org_id": orgid1,
       @"cyear": cyear1,
       @"url": mainstr1,
       @"tag": @"user_detail"
       };
    
    
    [Constant executequery:mainstr1 strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response student profile data:%@",maindic);
            
            self.studentId.text=[maindic objectForKey:@"studentId"];
            self.groupname.text=[maindic objectForKey:@"groupname"];
            self.mobile.text=[maindic objectForKey:@"mobile"];
            self.email.text=[maindic objectForKey:@"email"];
            self.dob.text=[maindic objectForKey:@"dob"];
            self.address.text=[maindic objectForKey:@"address"];
            self.firstName.text=[maindic objectForKey:@"first_name"];
            self.lastName.text=[maindic objectForKey:@"last_name"];
            self.instUrl.text=[[maindic objectForKey:@"instUrl"]description];
            self.course.text=[maindic objectForKey:@"course_name"];
            self.batch.text=[maindic objectForKey:@"batch_name"];
            self.semestername.text=[maindic objectForKey:@"semester_name"];
            self.section.text=[[maindic objectForKey:@"section_name"]description];
            self.username.text=[maindic objectForKey:@"username"];
            self.admissionNum.text=[maindic objectForKey:@"school_admission_no"];
            self.motherPhone.text=[maindic objectForKey:@"mother_mobile"];
            self.fatherPhone.text=[maindic objectForKey:@"father_mobile"];
            //self.profilepicImg.image=[maindic objectForKey:@"pic_id"];
            //self.empid.text=[maindic objectForKey:@"tag"];
            //self.empid.text=[maindic objectForKey:@"success"];
            //self.empid.text=[maindic objectForKey:@"error"];
            NSString *str4=[maindic objectForKey:@"pic_id"];
            NSLog(@"******pic_id**%@",str4);
            
            if(self.section.text==(NSString *) [NSNull null])
            {
                self.section.text=@"-";
            }
            if(self.semestername.text==(NSString *) [NSNull null])
            {
                self.semestername.text=@"-";
            }
            if(self.instUrl.text==(NSString *) [NSNull null])
            {
                self.instUrl.text=@"-";
            }
            if(self.dob.text==(NSString *) [NSNull null])
            {
                self.dob.text=@"-";
            }
            if(self.address.text==(NSString *) [NSNull null])
            {
                self.address.text=@"-";
            }
            //            if(self.instUrl.text== [NSNull null]){
            //                  self.instUrl.text=@"-";
            //            }
            //            if(self.section.text==[NSNull null]){
            //                self.section.text=@"-";
            //            }
            
            NSString *tempimgstr=str4;
            [_profilepicImg sd_setImageWithURL:[NSURL URLWithString:tempimgstr]
                              placeholderImage:[UIImage imageNamed:@"default.png"]];
            
            self.studentName.text = [NSString stringWithFormat: @"%@ %@", self.firstName.text,self.lastName.text];
            
            NSLog(@"studentId====%@ mobile num==%@",self.studentId.text,self.mobile.text);
            
            [[NSUserDefaults standardUserDefaults] setObject:_studentId.text forKey:@"studentId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];
    
}



- (IBAction)downBtnClicked:(UIButton *)sender {
    
    if (isSelected==YES) {
        [_downBtn setTitle:@"About" forState:UIControlStateNormal];
        [_downBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        isSelected=NO;
        _aboutView.hidden=YES;
        _contactView.hidden=NO;
        _upBtnLbl.text=@"Contact";
        
    }
    else{
        [_downBtn setTitle:@"Contact" forState:UIControlStateNormal];
        [_downBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        isSelected=YES;
        _contactView.hidden=YES;
        _aboutView.hidden=NO;
        _upBtnLbl.text=@"About";
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];
    
    
    studentIdtext.text = [@"STUDENT_ID" localize];
    sectiontxt.text=[@"SECTION" localize];
    coursetxt.text = [@"COURSE" localize];
    dateofbirthtxt.text=[@"DOB" localize];
    addresstxt.text=[@"HOME_ADDRESS" localize];
    emailtxt.text=[@"HOME_EMAIL" localize];
    batchtxt.text=[@"BATCH" localize];
    mobiletxt.text=[@"HOME_MOBILE" localize];
    aboutTxt.text=[@"ABOUT" localize];
    contactTxt.text=[@"CONTACT" localize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage:) name:@"notificationName" object:nil];
}

-(void)changeLanguage:(NSNotification*)notification
{
    studentIdtext.text = [@"STUDENT_ID" localize];
    sectiontxt.text=[@"SECTION" localize];
    coursetxt.text = [@"COURSE" localize];
    dateofbirthtxt.text=[@"DOB" localize];
    addresstxt.text=[@"HOME_ADDRESS" localize];
    emailtxt.text=[@"HOME_EMAIL" localize];
    batchtxt.text=[@"BATCH" localize];
    mobiletxt.text=[@"HOME_MOBILE" localize];
    aboutTxt.text=[@"ABOUT" localize];
    contactTxt.text=[@"CONTACT" localize];
}

- (void)viewDidLayoutSubviews{
    NSString *language = [@"" currentLanguage];
    if ([language isEqualToString:@"hi"])
    {
        [self setBackButtonLocalize];
    }
}

- (void)setBackButtonLocalize{
    self.navigationItem.title = [@"MY_PROFILE" localize];
}


@end
