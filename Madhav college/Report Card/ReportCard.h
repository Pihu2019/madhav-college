#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportCard : NSObject
@property (nonatomic,strong) NSString *examIdStr,*examPatternStr,*examTypeStr,*maxMarksStr,*minMarksStr,*examNameStr;
@end

NS_ASSUME_NONNULL_END
