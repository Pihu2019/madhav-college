

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportCardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *examId;
@property (weak, nonatomic) IBOutlet UILabel *examPattern;
@property (weak, nonatomic) IBOutlet UILabel *examType;
@property (weak, nonatomic) IBOutlet UILabel *maxMarks;
@property (weak, nonatomic) IBOutlet UILabel *minMarks;
@property (weak, nonatomic) IBOutlet UILabel *examName;
@property (weak, nonatomic) IBOutlet UILabel *pdf;




@property (nonatomic,strong) NSMutableArray *examIdArr,*examPatternArr,*examTypeArr,*maxMarksArr,*minMarksArr,*examNameArr,*reportArr;

@end

NS_ASSUME_NONNULL_END
