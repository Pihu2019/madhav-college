

#import "ReportCardCell.h"

@implementation ReportCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.pdf.layer.masksToBounds=YES;
    self.pdf.layer.cornerRadius=8;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
