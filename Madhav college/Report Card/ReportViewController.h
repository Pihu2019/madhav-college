
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReportViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,strong) NSMutableArray *examIdArr,*examPatternArr,*examTypeArr,*maxMarksArr,*minMarksArr,*examNameArr,*reportArr;

@property(nonatomic,retain)NSString *indxp,*examIdStr,*success,*tag,*error,*urlStr;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end

NS_ASSUME_NONNULL_END
