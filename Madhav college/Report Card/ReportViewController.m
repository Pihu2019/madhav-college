
#import "ReportViewController.h"
#import "ReportCardCell.h"
#import "ReportCard.h"
#import "Constant.h"
#import "Base.h"
#import "WebViewController.h"
@interface ReportViewController ()

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tableview setSeparatorColor:[UIColor clearColor]];
    _tableview.delegate=self;
    _tableview.dataSource=self;
    
 _indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
    _indicator.center=self.view.center;
    _indicator.backgroundColor=[UIColor lightGrayColor];
    [_indicator startAnimating];
    [self.indicator setHidden:NO];
    
    _examIdArr=[[NSMutableArray alloc]init];
    _examPatternArr=[[NSMutableArray alloc]init];
    _examTypeArr=[[NSMutableArray alloc]init];
    _maxMarksArr=[[NSMutableArray alloc]init];
    _minMarksArr=[[NSMutableArray alloc]init];
    _examNameArr=[[NSMutableArray alloc]init];
    _reportArr=[[NSMutableArray alloc]init];
    
    [_examIdArr removeAllObjects];
    [_examPatternArr removeAllObjects];
    [_examTypeArr removeAllObjects];
    [_maxMarksArr removeAllObjects];
    [_minMarksArr removeAllObjects];
    [_examNameArr removeAllObjects];
    [_reportArr removeAllObjects];
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in ==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@" username ==%@",username);
    
    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"admissionNum"];
    NSLog(@" admissionNum ==%@",admissionNum);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@" branchid ==%@",branchid);
    
    
    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:examReports]];
    
    NSDictionary *parameterDict = @{
                                    @"dbname":dbname,
                                    @"groupname":groupname,
                                    @"tag":@"view",
                                    @"school_admission_no":admissionNum,
                                    @"branch_id":branchid,
                                    @"username":username
                            
                                    };
    NSLog(@" parameterDict data:%@",parameterDict);
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response data:%@",maindic);
            
            NSArray *ciculararr=[maindic objectForKey:@"exam_result"];
            NSLog(@"exam_result :%@",ciculararr);
            
            if(ciculararr.count==0)
            {
                
                UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"No data available" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertView dismissViewControllerAnimated:YES completion:nil];
                                     }];
                [alertView addAction:ok];
                [self presentViewController:alertView animated:YES completion:nil];
            }
            else {
                

                    for(NSDictionary *temp in ciculararr)
                    {
                        NSString *str1=[temp objectForKey:@"exam_id"];
                        NSString *str2=[[temp objectForKey:@"exam_pattern"]description];
                        NSString *str3=[[temp objectForKey:@"exam_type"]description];
                        NSString *str4=[[temp objectForKey:@"max_marks"]description];
                        NSString *str5=[[temp objectForKey:@"min_marks"]description];
                        NSString *str6=[[temp objectForKey:@"exam_name"]description];
                        
                        
                        NSLog(@"exam_id=%@  exam_pattern=%@ exam type=%@ max_marks=%@ min_marks=%@ exam_name=%@",str1,str2,str3,str4,str5,str6);
                        if (([(NSString*)str1 isEqual: [NSNull null]])) {
    
                        }else {
                            
                            ReportCard *k1=[[ReportCard alloc]init];
                            k1.examIdStr=str1;
                            k1.examPatternStr=str2;
                            k1.examTypeStr=str3;
                            k1.maxMarksStr=str4;
                            k1.minMarksStr=str5;
                            k1.examNameStr=str6;
                           
                            
                            
                            if ([k1.minMarksStr containsString:@"<null>"]){
                                k1.minMarksStr=@"0";
                            }
                            if ([k1.maxMarksStr containsString:@"<null>"]){
                                k1.maxMarksStr=@"0";
                            }
                            [_reportArr addObject:k1];
                        }
                        [_tableview reloadData];
                    
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableview reloadData];
            
            [self.indicator stopAnimating];
            [self.indicator setHidden:YES];
        });
    }];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _reportArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        ReportCardCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        ReportCard *ktemp=[_reportArr objectAtIndex:indexPath.row];
    
        cell.examId.text=ktemp.examIdStr;
        cell.examPattern.text=ktemp.examPatternStr;
        cell.examType.text=ktemp.examTypeStr;
        cell.maxMarks.text=ktemp.maxMarksStr;
        cell.minMarks.text=ktemp.minMarksStr;
        cell.examName.text=ktemp.examNameStr;
    
        NSLog(@" examType ==%@",ktemp.examTypeStr);
        
        
        return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 77.0f;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [_indicator startAnimating];
    [self.indicator setHidden:NO];
    ReportCardCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    NSLog(@"cell==%@",cell);
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];

    _examIdStr=cell.examId.text;
    
    [[NSUserDefaults standardUserDefaults] setObject:_examIdStr forKey:@"examId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"_examIdStr==%@",_examIdStr);
     [self pdfReportParsing];

}
-(void)pdfReportParsing{
    
    NSString *examId = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"examId"];
    NSLog(@"examId==%@",examId);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@" branchid ==%@",branchid);
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in ==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@" username ==%@",username);
    
    NSString *admissionNum = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"admissionNum"];
    NSLog(@" admissionNum ==%@",admissionNum);
    
    
    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:examReports]];
    
    NSDictionary *parameterDict =
    @{ @"groupname":groupname,
       @"username": username,
       @"dbname":dbname,
       @"branch_id":branchid,
       @"instUrl":instUrl,
       @"tag": @"getPdf",
       @"school_admission_no":admissionNum,
       @"exam_id":examId
       };
    
     NSLog(@"parameter dict%@",parameterDict);
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        //  NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
             NSLog(@"response exam record details data:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[maindic objectForKey:@"success"];
            _error=[maindic objectForKey:@"error"];
            _urlStr=[maindic objectForKey:@"url"];
            
            NSLog(@"tag==%@& success=%@ url str=%@",_tag,_success,_urlStr);
            
            [[NSUserDefaults standardUserDefaults] setObject:_urlStr forKey:@"pdfurlStr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"***pdfurlStr***** = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"pdfurlStr"]);
            
            
            [self performSegueWithIdentifier:@"showpdfAttachment" sender:nil];
            
                [_indicator stopAnimating];
                [self.indicator setHidden:YES];
            
//            if([self.success isEqualToString:@"1"])
//            {
//
//                [self performSegueWithIdentifier:@"showpdfAttachment" sender:nil];
//                NSLog(@"****succesffullyy");
//
//            }
//            else
//            {
//
//            }

        }
    }];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSString *pdfurlStr = [[NSUserDefaults standardUserDefaults]stringForKey:@"pdfurlStr"];
    NSLog(@"***pdfurlStr ==%@",pdfurlStr);
    
    WebViewController *wvc=[segue destinationViewController];
    if ([segue.identifier isEqualToString:@"showpdfAttachment"]) {
        wvc.myURL=pdfurlStr;
    }
}
@end
