
#import <Foundation/Foundation.h>

@interface NSString (Language)
- (NSString *)localize;
- (NSString *)currentLanguage;
@end
