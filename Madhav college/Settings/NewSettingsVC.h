
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewSettingsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *facebookBtnClicked;
@property (weak, nonatomic) IBOutlet UIButton *instagramBtn;
@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;

- (IBAction)fbBtnClicked:(id)sender;
- (IBAction)instaBtnClicked:(id)sender;
- (IBAction)subscribeBtnClicked:(id)sender;

@end

NS_ASSUME_NONNULL_END
