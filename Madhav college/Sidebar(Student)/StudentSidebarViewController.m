

#import "StudentSidebarViewController.h"
#import "Constant.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BaseViewController.h"
#import "Base.h"
#import "SubscribeViewController.h"
#import "TransportViewController.h"
@interface StudentSidebarViewController ()

@end

@implementation StudentSidebarViewController
@synthesize dashboardTxt;
@synthesize courseTxt;
@synthesize settingTxt;
@synthesize timetableTxt;
@synthesize transportTxt;
@synthesize leavesTxt;
@synthesize attendanceTxt;
@synthesize hostelTxt;
@synthesize libraryTxt;
@synthesize logoutTxt;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width / 2;
    self.profileImg.clipsToBounds = YES;
//    self.profileImg.layer.borderWidth = 1.0f;
//    self.profileImg.layer.borderColor = [UIColor lightGrayColor].CGColor;

    self.profileView.layer.cornerRadius = self.profileView.frame.size.width / 2;
    self.profileView.clipsToBounds = YES;
    
    [_scrollview setShowsHorizontalScrollIndicator:NO];
    [_scrollview setShowsVerticalScrollIndicator:NO];
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"transportLicense"];
    
    
     [self getProfile];
    
}
-(void)getProfile{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    
    NSString *mainstr1=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:profile]];
    
    
    NSDictionary *parameterDict =
    @{ @"groupname":groupname,
       @"username":username,
       @"dbname":dbname,
       @"Branch_id":branchid,
       @"org_id": orgid,
       @"cyear": cyear,
       @"url":mainstr1,
       @"tag":@"user_detail",
       @"password": password};
    
    
    [Constant executequery:mainstr1 strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response student profile data:%@",maindic);
        
          //  self.email.text=[maindic objectForKey:@"email"];
            self.firstname.text=[maindic objectForKey:@"first_name"];
            self.lastname.text=[maindic objectForKey:@"last_name"];
            self.username.text=[maindic objectForKey:@"username"];
    
            NSString *str4=[maindic objectForKey:@"pic_id"];
            NSLog(@"******pic_id**%@",str4);
            NSString *tempimgstr=str4;
            [_profileImg sd_setImageWithURL:[NSURL URLWithString:tempimgstr]
                             placeholderImage:[UIImage imageNamed:@"default.png"]];
            
            self.studentname.text = [NSString stringWithFormat: @"%@ %@", self.firstname.text,self.lastname.text];
            
            NSLog(@"studentname====%@",self.studentname.text);
        }
    }];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidLoad];

    dashboardTxt.text = [@"DASHBOARD" localize];
    courseTxt.text=[@"COURSE" localize];
    settingTxt.text=[@"SETTINGS" localize];
    timetableTxt.text=[@"TIMETABLE" localize];    transportTxt.text=[@"TRANSPORT_ROUTE" localize];
    leavesTxt.text=[@"LEAVES" localize];
    attendanceTxt.text=[@"ATTENDANCE" localize];
    hostelTxt.text=[@"HOSTELS" localize];
    libraryTxt.text=[@"LIBRARY_PANEL" localize];
    logoutTxt.text=[@"LOGOUT" localize];
    _homeworkTxt.text=[@"HOMEWORK" localize];
    _gatepassTxt.text=[@"GATEPASS" localize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeLanguage:) name:@"notificationName" object:nil];
}

-(void)changeLanguage:(NSNotification*)notification
{
    dashboardTxt.text = [@"DASHBOARD" localize];
    courseTxt.text=[@"COURSE" localize];
    settingTxt.text=[@"SETTINGS" localize];
    timetableTxt.text=[@"TIMETABLE" localize];    transportTxt.text=[@"TRANSPORT_ROUTE" localize];
    leavesTxt.text=[@"LEAVES" localize];
    attendanceTxt.text=[@"ATTENDANCE" localize];
    hostelTxt.text=[@"HOSTELS" localize];
    libraryTxt.text=[@"LIBRARY_PANEL" localize];
    logoutTxt.text=[@"LOGOUT" localize];
    _homeworkTxt.text=[@"HOMEWORK" localize];
    _gatepassTxt.text=[@"GATEPASS" localize];

}
- (IBAction)transportBtnClicked:(id)sender {
     NSLog(@"**TRANSPORT Button clicked***");
    
    NSString *transportLicense = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"transportLicense"];
    NSLog(@"TRANSPORT Licence==%@",transportLicense);
    if ([transportLicense isEqual:@"0"])
    {
        NSLog(@"subscribe ==");

        SubscribeViewController *transportZero = [self.storyboard instantiateViewControllerWithIdentifier:@"transportZero"];
        
        [self.navigationController pushViewController:transportZero animated:YES];
    }
    else if ([transportLicense isEqual:@"1"])
    {
        NSLog(@"transport ==");

        TransportViewController *transportOne = [self.storyboard instantiateViewControllerWithIdentifier:@"transportOne"];
        
        [self.navigationController pushViewController:transportOne animated:YES];
    }
    
}
- (IBAction)logoutBtnClicked:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
