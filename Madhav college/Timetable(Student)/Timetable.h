
#import <Foundation/Foundation.h>

@interface Timetable : NSObject

@property (nonatomic,strong) NSString *lectureTimingStr,*lectureNameStr,*subjectnameStr,*subjectIdStr,*dayStr,*teacherNameStr,*breakStr;
@property (strong,nonatomic) NSString *day;
@end
