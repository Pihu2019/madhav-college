

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubscribeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *amount;
@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;
- (IBAction)subscribeBtnClicked:(id)sender;

@property (nonatomic,strong) NSString *success,*error,*tag;
@property (weak, nonatomic) IBOutlet UIView *subscribeView;
@end

NS_ASSUME_NONNULL_END
