

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Subscription : NSObject
@property (nonatomic,strong) NSString *etracki_fees,*maxNoOfBuses,*licenceActivationDate,*licenceDeactivationDate,*txnId,*studentId;
@end

NS_ASSUME_NONNULL_END
