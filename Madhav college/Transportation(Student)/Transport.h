
#import <Foundation/Foundation.h>

@interface Transport : NSObject
@property (nonatomic,strong) NSString *stopNameStr,*pickdropStatus,*routeIdStr,*routeNameStr,*stopIdStr,*latitudeStr,*longitudeStr,*journeyDetailsStr;


@property (nonatomic,strong) NSString *driverIdStr,*driverNameStr,*vehicleNumStr,*vehicleIdStr,*vehicleNameStr,*journeyIdStr,*stopStr,*routeIdStr1,*addressStr;
@end
