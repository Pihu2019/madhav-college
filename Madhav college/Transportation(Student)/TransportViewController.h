

#import <UIKit/UIKit.h>

@interface TransportViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic,strong) NSMutableArray *transportArr,*routeIdArr,*routeNameArr,*pickDropArr,*stopIdArr,*stopName,*latitudeArr,*longitudeArr,*journeyDetailArr;

@property(nonatomic,retain)NSString *indxp,*journeyId,*stopNameStr,*latitudeStr,*longitudeStr,*addressStr;

@end
