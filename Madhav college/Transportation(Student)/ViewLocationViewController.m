

//GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:21.1450677 longitude:79.0889168 zoom:14 bearing:0 viewingAngle:0];
//https://stackoverflow.com/questions/13433234/sending-nsnumber-strong-to-parameter-of-incompatible-type-cllocationdegree/13433261

#import "ViewLocationViewController.h"
#import "CurrentLocation.h"
#import <GoogleMaps/GoogleMaps.h>
#import "UIKit/UIKit.h"
#import "Base.h"
#import "Transport.h"
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface ViewLocationViewController ()
{
    NSTimer *timer;
}

@property (weak, nonatomic) IBOutlet UIView *mapbackView;
@property(strong,nonatomic) GMSMapView *mapView;
@end

@implementation ViewLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   _locationArr=[[NSMutableArray alloc]init];
   _transportArr=[[NSMutableArray alloc]init];
    NSLog(@"LATITUDE:: %@ LONGITUDE:: %@",_latitudeStr,_longitudeStr);
    self.journeyId.text=_journeyIdStr;
   
    [self parsingTransportationData];
    
    //GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:21.1450677 longitude:79.0889168 zoom:14 bearing:0 viewingAngle:0];
   GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:[_latitudeStr doubleValue] longitude:[_longitudeStr doubleValue] zoom:14];

    self.mapView=[GMSMapView mapWithFrame:self.view.bounds camera:camera];
    
    self.mapView.mapType=kGMSTypeNormal;
    //self.mapView.myLocationEnabled=YES;//to find current location
    self.mapView.settings.compassButton=YES;
    self.mapView.settings.myLocationButton=YES;
    [self.mapView setMinZoom:10 maxZoom:18];
    
    [self.mapbackView addSubview:self.mapView];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                      // [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(getCurrentRouteList) userInfo:nil repeats:YES];
                       
                           if (timer==nil) {
                               timer=[NSTimer scheduledTimerWithTimeInterval:10.0 repeats:true block:^(NSTimer * _Nonnull timer) {
                                   [self getCurrentRouteList];
                                   NSLog(@"CURRENT ROUTE LIST");
                               }];
                           }
                    
                       GMSMarker *marker=[[GMSMarker alloc]init];
                       marker.position=CLLocationCoordinate2DMake([_latitudeStr doubleValue],[_longitudeStr doubleValue]);
                      // marker.position=CLLocationCoordinate2DMake(21.1450677,79.0889168);
                       marker.map=self.mapView;
                       marker.title=self.stopNameStr;
                       marker.snippet=self.addressStr;
                       marker.appearAnimation=kGMSMarkerAnimationPop;
                       // marker.icon=[GMSMarker markerImageWithColor:[UIColor redColor]];
                       marker.icon=[UIImage imageNamed:@"map_car_running.png"];
                       
                       GMSMutablePath *path = [GMSMutablePath path];
                       [path addCoordinate:CLLocationCoordinate2DMake([_latitudeStr doubleValue],[_longitudeStr doubleValue])];
                       
                       NSString *newlatitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlatitudeStr"];
                       NSLog(@"newlatitudeStr ==%@",newlatitudeStr);
                       
                       NSString *newlongitudeStr = [[NSUserDefaults standardUserDefaults] stringForKey:@"newlongitudeStr"];
                       NSLog(@"newlongitudeStr ==%@",newlongitudeStr);
                       
                       [path addCoordinate:CLLocationCoordinate2DMake([newlatitudeStr doubleValue],[newlongitudeStr doubleValue])];
                       
//                       GMSPolyline *rectangle = [GMSPolyline polylineWithPath:path];
//                       rectangle.strokeWidth = 2.f;
//                       rectangle.map = self.mapView;
//                       [self.mapbackView addSubview:rectangle.map];
                       
                   });
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [locationManager requestWhenInUseAuthorization];
    }
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
   
    
//    if (timer==nil) {
//        timer=[NSTimer scheduledTimerWithTimeInterval:10.0 repeats:true block:^(NSTimer * _Nonnull timer) {
//            [self getCurrentRouteList];
//        }];
//    }
    
   
   
}
-(void)getCurrentRouteList{
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    //NSString *mystr=[NSString stringWithFormat:@"journey_id=671&Branch_id=%@&tag=Current_Location_of_Vehicle",branchid];
    
    NSString *mystr=[NSString stringWithFormat:@"journey_id=%@&admission_no=%@&Branch_id=%@&tag=Current_Location_of_Vehicle",_journeyIdStr,username,branchid];
    
    
    NSLog(@"parameterDict in currnt route list%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
   // NSString *mainstr1=[NSString stringWithFormat:@"%@",[subscriptionUrl stringByAppendingString:currentroutelist]];
    
   //  NSString *mainstr1=[NSString stringWithFormat:@"http://erp.eshiksa.net/edemo_fees/esh/plugins/APIS/currentroutelist.php"];
    
       NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/currentroutelist.php"];
    
    NSURL * url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSLog(@"text==%@",text);
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSDictionary *dic=[responseDict objectForKey:@"location"];
                                         NSLog(@"location dic%@",dic);
                                         
                                         if(dic.count==0)
                                         {
                                             NSLog(@"CURRENT location STOPPED***");
                                             
                                         }
                                         else {
                                         
                                         CurrentLocation *c=[[CurrentLocation alloc]init];
                                         
                                         c.driverIdStr=[dic objectForKey:@"driver_id"];
                                         c.busIdStr=[dic objectForKey:@"bus_id"];
                                         c.cdatetimeStr=[dic objectForKey:@"cdatetime"];
                                         c.latitudeStr=[dic objectForKey:@"latetude"];
                                         c.longitudeStr=[dic objectForKey:@"longitude"];
                                         c.angleStr=[dic objectForKey:@"angle"];
                                         c.opyearStr=[dic objectForKey:@"opyear"];
                                         c.journeyIdStr=[dic objectForKey:@"journey_id"];
                                         
                                         
                                         NSLog(@"in transport driverIdStr==%@ longitudeStr ==%@",c.driverIdStr,c.longitudeStr);
                                             
                                             [[NSUserDefaults standardUserDefaults] setObject:c.latitudeStr forKey:@"newlatitudeStr"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             [[NSUserDefaults standardUserDefaults] setObject:c.longitudeStr forKey:@"newlongitudeStr"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             
                                            // [_mapView animateToLocation:CLLocationCoordinate2DMake([c.latitudeStr doubleValue],[c.longitudeStr doubleValue])];
                                             
                                            //comment GMSPloyline if animate this method is used
                                         }
                                     }];
    
    [dataTask resume];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // Turn off the location manager to save power.
    [locationManager stopUpdatingLocation];
}

-(BOOL)prefersStatusBarHidden{
    return  YES;
}

-(void)parsingTransportationData{
    
    [_transportArr removeAllObjects];
    [_locationArr removeAllObjects];
    
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *mystr=[NSString stringWithFormat:@"admission_no=%@&Branch_id=%@&tag=Stop_Route_list",username,branchid];
    
    NSLog(@"parameterDict%@",mystr);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    
   // NSString *mainstr1=[NSString stringWithFormat:@"%@",[subscriptionUrl stringByAppendingString:currentroutelist]];
    // NSString *mainstr1=[NSString stringWithFormat:@"http://erp.eshiksa.net/edemo_fees/esh/plugins/APIS/currentroutelist.php"];
    
    NSString *mainstr1=[NSString stringWithFormat:@"http://eps.eshiksa.net/esh/plugins/APIS/currentroutelist.php"];
    
    NSURL * url = [NSURL URLWithString:mainstr1];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[mystr dataUsingEncoding:NSUTF8StringEncoding]];
    NSError *error=nil;
    if(error)
    {
    }
    NSURLSessionDataTask *dataTask =[defaultSession dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         if(error)
                                         {
                                         }
                                         
                                         NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                         NSLog(@"text==%@",text);
                                         NSError *er=nil;
                                         NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&er];
                                         if(er)
                                         {
                                         }
                                         NSLog(@"responseDict:%@",responseDict);
                                         
                                         NSArray *routelist=[responseDict objectForKey:@"route_list"];
                                         
                                         NSLog(@"route_list:%@",routelist);
                                         
                                         if(routelist.count==0)
                                         {
                                             UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"No data available" preferredStyle:UIAlertControllerStyleAlert];
                                             
                                             UIAlertAction* ok = [UIAlertAction
                                                                  actionWithTitle:@"OK"
                                                                  style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action)
                                                                  {
                                                                      [alertView dismissViewControllerAnimated:YES completion:nil];
                                                                      
                                                                  }];
                                             
                                             
                                             [alertView addAction:ok];
                                             
                                             [self presentViewController:alertView animated:YES completion:nil];
                                             
                                         }
                                         else {
                                             
                                             for(NSDictionary *temp1 in routelist)
                                             {
                                                 NSString *str1=[[temp1 objectForKey:@"route_id"]description];
                                                 NSString *str2=[[temp1 objectForKey:@"route_name"]description];
                                                 NSString *str3=[temp1 objectForKey:@"pick_drop"];
                                                 NSString *str4=[temp1 objectForKey:@"stop_id"];
                                                 NSString *str5=[[temp1 objectForKey:@"stop_name"]description];
                                                 NSString *str6=[[temp1 objectForKey:@"latitude"]description];
                                                 NSString *str7=[temp1 objectForKey:@"longitude"];
                                                 NSString *str8=[temp1 objectForKey:@"journey_details"];
                                                 
                                                 
                                                 NSLog(@"route_id=%@  route_name=%@ pick_drop=%@ stop_name=%@",str1,str2,str3,str5);
                                                 
                                                 
                                                 Transport *k1=[[Transport alloc]init];
                                                 k1.routeIdStr=str1;
                                                 k1.routeNameStr=str2;
                                                 k1.pickdropStatus=str3;
                                                 k1.stopIdStr=str4;
                                                 k1.stopNameStr=str5;
                                                 k1.latitudeStr=str6;
                                                 k1.longitudeStr=str7;
                                                 k1.journeyDetailsStr=str8;
                                                 
                                                 
                                                 NSDictionary *dic=[temp1 objectForKey:@"journey_details"];
                                                 
                                                 k1.driverIdStr=[dic objectForKey:@"driver_id"];
                                                 k1.driverNameStr=[dic objectForKey:@"driver_name"];
                                                 k1.vehicleNumStr=[dic objectForKey:@"vechile_no"];
                                                 k1.vehicleIdStr=[dic objectForKey:@"vechile_id"];
                                                 k1.vehicleNameStr=[dic objectForKey:@"vechile_name"];
                                                 k1.journeyIdStr=[dic objectForKey:@"journey_id"];
                                                 k1.routeIdStr=[dic objectForKey:@"route_id"];
                                                 k1.stopStr=[dic objectForKey:@"stops"];
                                                 
                                                 NSLog(@"driverNameStr:%@  vehicleNumStr:%@ stops%@",k1.driverNameStr,k1.vehicleNumStr,k1.stopStr);
                                                 
                                                 [_transportArr addObject:k1];
                                                 
                                                 
                                                 
                                                 NSArray *subarr=[temp1 objectForKey:@"stops"];
                                                 
                                                 NSLog(@"****stops arr:%@",subarr);
                                                 NSMutableDictionary *temp=[[NSMutableDictionary alloc]init];
                                                 for(temp in subarr)
                                                 {
                                                     NSString *str1=[[temp objectForKey:@"Branch_id"]description];
                                                     NSString *str2=[[temp objectForKey:@"stop_id"]description];
                                                     NSString *str3=[[temp objectForKey:@"route_id"]description];
                                                     NSString *str4=[[temp objectForKey:@"stop_name"]description];
                                                     NSString *str5=[[temp objectForKey:@"lankmark"]description];
                                                     NSString *str6=[[temp objectForKey:@"landmark_address"]description];
                                                     NSString *str7=[[temp objectForKey:@"lat"]description];
                                                     NSString *str8=[[temp objectForKey:@"landmark_lat"]description];
                                                     NSString *str9=[[temp objectForKey:@"lng"]description];
                                                     NSString *str10=[[temp objectForKey:@"landmark_lng"]description];
                                                     NSString *str11=[[temp objectForKey:@"view_order"]description];
                                                     NSString *str12=[[temp objectForKey:@"fees_amount"]description];
                                                     NSString *str13=[[temp objectForKey:@"created_date"]description];
                                                     NSString *str14=[[temp objectForKey:@"status"]description];
                                                     NSString *str15=[[temp objectForKey:@"opyear"]description];
                                                     NSString *str16=[[temp objectForKey:@"org_id"]description];
                                                     NSString *str17=[[temp objectForKey:@"address"]description];
                                                     NSString *str18=[[temp objectForKey:@"pincode"]description];
                                                     
                                                     NSLog(@"Branch_id=%@  stop_id=%@ stop_name=%@ route_id=%@ landmark=%@ landmark_address=%@  lat=%@ landmark_lat=%@ lng=%@ landmark_lng=%@ view_order=%@  fees_amount=%@ created_date=%@ status=%@ opyear=%@ org_id=%@ address=%@ pincode=%@",str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12,str13,str14,str15,str16,str17,str18);
                                                    
                                                     [self.locationArr addObject:temp];
                                                 }
                                                 GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                                                 CLLocationCoordinate2D location;
                                                 for (temp in _locationArr)
                                                 {
                                                     location.latitude = [temp[@"landmark_lat"] floatValue];
                                                     location.longitude = [temp[@"landmark_lng"] floatValue];
                                                     // Creates a marker in the center of the map.
                                                     GMSMarker *marker = [[GMSMarker alloc] init];
                                                     //marker.icon=[GMSMarker markerImageWithColor:[UIColor redColor]];
                                                     marker.icon=[UIImage imageNamed:@"map_car_running.png"];
                                                     marker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude);
                                                     bounds = [bounds includingCoordinate:marker.position];
                                                     marker.title = temp[@"stop_name"];
                                                     marker.map = self.mapView;
                                                 }
                                                 [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
                                             }
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                             });
                                             }
                            }];
    [dataTask resume];
    
}


/*
double lat = #your latitude#;
double lng = #your longitude#;
double latEnd = #your end latitude#;
double lngEnd = #your end longitude#;
NSString *directionsUrlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?&origin=%f,%f&destination=%f,%f&mode=driving", lat, lng, latEnd, lngEnd];
NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];


NSURLSessionDataTask *mapTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                 {
                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                     if(error)
                                     {
                                         if(completionHandler)
                                             completionHandler(nil);
                                         return;
                                     }
                                     
                                     NSArray *routesArray = [json objectForKey:@"routes"];
                                     
                                     GMSPolyline *polyline = nil;
                                     if ([routesArray count] > 0)
                                     {
                                         NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                         NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                         NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                         GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                         polyline = [GMSPolyline polylineWithPath:path];
                                     }
                                     
                                     // run completionHandler on main thread
                                     dispatch_sync(dispatch_get_main_queue(), ^{
                                         if(completionHandler)
                                             completionHandler(polyline);
                                     });
                                 }];
[mapTask resume];
}*/
@end
